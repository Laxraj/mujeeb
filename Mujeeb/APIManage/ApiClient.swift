//
//  ApiClient.swift
//  Medics2you
//
//  Created by Priyanka Navadiya on 14/07/20.
//  Copyright © 2020 Techwin iMac-2. All rights reserved.
//

import UIKit
import Alamofire

class ApiClient: NSObject {
    

    let httpHeaders: HTTPHeaders = ["Content-Type": "application/json",
    "Accept":"application/json"]
    

    
           
    
    static let shared = ApiClient()
    
    private override init() {
        super.init()
    }

    func callPostUrl(urlString:String, param: Parameters, complition: @escaping(AFDataResponse<Any>)-> Void, failure: @escaping (String) -> Void) {
        if (UtilityClass.manager.isConnectedToNetwork()){
            
            guard let jsonData = try? JSONSerialization.data(withJSONObject: param, options: .prettyPrinted) else {
                return
            }
            
            let url = NSURL(string: urlString)
            var request = URLRequest(url: url! as URL)
            request.httpMethod = HTTPMethod.post.rawValue
           
            request.httpBody = jsonData
            
            AF.request(request).responseJSON { (response) in
                if(response.response?.statusCode == 401) {
//                    if UserDefaults().dictionary(forKey: PROFILE_DATA) != nil{
//                       self.pushToLoginScreen(responseData: response)
//                    }

//                    failure(err.localizedDescription)
                    complition(response)
                } else {
                    switch response.result {
                    case .success:
                        print(response.value)
                        complition(response)
                    case .failure:
                        if let err = response.error {
                            failure(err.localizedDescription)
                        }
                    }
                }
            }
        } else {
            failure("No internet connection!")
        }
    }
    
    
    func callGetUrlWithHeaders(urlString:String, complition: @escaping(AFDataResponse<Any>)-> Void, failure: @escaping (String) -> Void) {
        if (UtilityClass.manager.isConnectedToNetwork()){
            
         
            
            let url = NSURL(string: urlString)
            var request = URLRequest(url: url! as URL)
            request.httpMethod = HTTPMethod.get.rawValue
            
            
            AF.request(request).responseJSON { (response) in
//                if(response.response?.statusCode == 401) {
//                    self.pushToLoginScreen(responseData: response)
////                    self.newAccessTokenPostUrl(url: urlString, parameters: [:])
//                } else {
                    switch response.result {
                    case .success:
                        print(response.value)
                        complition(response)
                    case .failure:
                        if let err = response.error {
                            failure(err.localizedDescription)
                        }
                    }
//                }
            }
        } else {
            failure("No internet connection!")
        }
    }
    
    func callGetUrl(urlString:String, complition: @escaping(AFDataResponse<Any>)-> Void, failure: @escaping (String) -> Void) {
        if (UtilityClass.manager.isConnectedToNetwork()){
            
            var request = URLRequest(url: URL(string: urlString)!)
            request.httpMethod = HTTPMethod.get.rawValue
            
            AF.request(request).responseJSON { (response) in
                if(response.response?.statusCode == 401) {
                   // self.pushToLoginScreen(responseData: response)
                } else {
                    switch response.result {
                    case .success:
                          print(response.value)
                        complition(response)
                    case .failure:
                        if let err = response.error {
                            failure(err.localizedDescription)
                        }
                    }
                }
            }
        } else {
            failure("No internet connection!")
        }
    }
    func callGetUrlWithQuery(urlString:String, param: Parameters, complition: @escaping(AFDataResponse<Any>)-> Void, failure: @escaping (String) -> Void) {
        if (UtilityClass.manager.isConnectedToNetwork()){
            AF.request(urlString, method: .get, parameters: param, encoding:  URLEncoding.default, headers: httpHeaders).responseJSON { (response) in
                if(response.response?.statusCode == 401) {
                   
                    complition(response)
                } else {
                    switch response.result {
                    case .success:
                        print(response.value)
                        complition(response)
                    case .failure:
                        if let err = response.error {
                            failure(err.localizedDescription)
                        }
                    }
                }
            }
        }
    }
    
    func callPostUrlWithQueryParam(urlString:String, param: Parameters, complition: @escaping(AFDataResponse<Any>)-> Void, failure: @escaping (String) -> Void) {
        if (UtilityClass.manager.isConnectedToNetwork()){
            
            let url = URL(string: urlString)!
            AF.request(url , method: .post, parameters: param, headers: httpHeaders).responseJSON { (response) in
                if(response.response?.statusCode == 401) {
                   // self.pushToLoginScreen(responseData: response)
                } else {
                    switch response.result {
                    case .success:
                        complition(response)
                    case .failure:
                        if let err = response.error {
                            failure(err.localizedDescription)
                        }
                    }
                }
            }
        } else {
            failure("No internet connection!")
        }
    }
    

    

}


struct ApiFailure: Codable {
    let message: String?
    let status: Bool?
    let error: String?
}
