//
//  CategoryApiManager.swift
//  Mujeeb
//
//  Created by iroid on 04/11/20.
//  Copyright © 2020 iroid. All rights reserved.
//


import Foundation
import Alamofire

class CategoryApiManager: NSObject {
    
    static let shared = CategoryApiManager()
    let RESPONSE_ERROR_MSG = "JSON is not in correct format, something went wrong"
    let SOMETHING_WENT_WRONG = "something went wrong"
    private override init() {
        super.init()
    }
    
    func getCategory(url:String , complition: @escaping ([CategoryData]) -> Void, failure: @escaping (String) -> Void) {
        ApiClient.shared.callGetUrl(urlString: url, complition: { (response) in
         
            if(response.response?.statusCode == 200) {
                
                guard let responseData = try? JSONDecoder().decode([CategoryData].self, from: response.data!) else {
                    failure(self.RESPONSE_ERROR_MSG)
                    return
                }
                complition(responseData)
            } else {
                guard let responseData = try? JSONDecoder().decode(LoginFailure.self, from: response.data!) else {
                    failure(self.RESPONSE_ERROR_MSG)
                    return
                }
                var message = self.SOMETHING_WENT_WRONG
                
                if let msg = responseData.message {
                    message = msg
                }
                failure(message)
            }
        }) { (msg) in
            failure(msg)
        }
    }
    
    func getInnerCategory(url:String , complition: @escaping (InnerCategoryData) -> Void, failure: @escaping (String) -> Void) {
        ApiClient.shared.callGetUrl(urlString: url, complition: { (response) in
         
            if(response.response?.statusCode == 200) {
                
                guard let responseData = try? JSONDecoder().decode(InnerCategoryData.self, from: response.data!) else {
                    failure(self.RESPONSE_ERROR_MSG)
                    return
                }
                complition(responseData)
            } else {
                guard let responseData = try? JSONDecoder().decode(LoginFailure.self, from: response.data!) else {
                    failure(self.RESPONSE_ERROR_MSG)
                    return
                }
                var message = self.SOMETHING_WENT_WRONG
                
                if let msg = responseData.message {
                    message = msg
                }
                failure(message)
            }
        }) { (msg) in
            failure(msg)
        }
    }
      func login(url:String, parameter: Parameters, complition: @escaping (InnerCategoryData) -> Void, failure: @escaping (String) -> Void) {
            
            ApiClient.shared.callPostUrlWithQueryParam(urlString: url, param: parameter, complition: { (response) in
                
                if(response.response?.statusCode == 200) {
                    guard let responseData = try? JSONDecoder().decode(InnerCategoryData.self, from: response.data!) else {
                        failure(self.RESPONSE_ERROR_MSG)
                        return
                    }
    //                if responseData.status {
                        complition(responseData)
    //                }
                } else {
                    guard let responseData = try? JSONDecoder().decode(LoginFailure.self, from: response.data!) else {
                        failure(self.RESPONSE_ERROR_MSG)
                        return
                    }
                    var message = self.SOMETHING_WENT_WRONG
                    
                    if let msg = responseData.message {
                        message = msg
                    }
                    failure(message)
                }
            }) { (msg) in
                failure(msg)
            }
        }
    
    func getNewsHomePage(url:String , complition: @escaping (NewsModelHomeData) -> Void, failure: @escaping (String) -> Void) {
        ApiClient.shared.callGetUrl(urlString: url, complition: { (response) in
         
            if(response.response?.statusCode == 200) {
                
                guard let responseData = try? JSONDecoder().decode(NewsModelHomeData.self, from: response.data!) else {
                    failure(self.RESPONSE_ERROR_MSG)
                    return
                }
                complition(responseData)
            } else {
                guard let responseData = try? JSONDecoder().decode(LoginFailure.self, from: response.data!) else {
                    failure(self.RESPONSE_ERROR_MSG)
                    return
                }
                var message = self.SOMETHING_WENT_WRONG
                
                if let msg = responseData.message {
                    message = msg
                }
                failure(message)
            }
        }) { (msg) in
            failure(msg)
        }
    }
    
    func getBannerHomePage(url:String , complition: @escaping (BannerModelHomeData) -> Void, failure: @escaping (String) -> Void) {
        ApiClient.shared.callGetUrl(urlString: url, complition: { (response) in
         
            if(response.response?.statusCode == 200) {
                
                guard let responseData = try? JSONDecoder().decode(BannerModelHomeData.self, from: response.data!) else {
                    failure(self.RESPONSE_ERROR_MSG)
                    return
                }
                complition(responseData)
            } else {
                guard let responseData = try? JSONDecoder().decode(LoginFailure.self, from: response.data!) else {
                    failure(self.RESPONSE_ERROR_MSG)
                    return
                }
                var message = self.SOMETHING_WENT_WRONG
                
                if let msg = responseData.message {
                    message = msg
                }
                failure(message)
            }
        }) { (msg) in
            failure(msg)
        }
    }
    func addAdv(url:String,param:Parameters , complition: @escaping (Success) -> Void, failure: @escaping (String) -> Void) {
           ApiClient.shared.callGetUrlWithQuery(urlString: url, param: param, complition: { (response) in
               if(response.response?.statusCode == 200) {
                   guard let responseData = try? JSONDecoder().decode(Success.self, from: response.data!) else {
                       failure(self.RESPONSE_ERROR_MSG)
                       return
                   }
                   complition(responseData)
                
               }else if response.response?.statusCode == 401{
                   guard let responseData = try? JSONDecoder().decode(LoginFailure.self, from: response.data!) else {
                       failure(self.RESPONSE_ERROR_MSG)
                       return
                   }
                   var message = self.SOMETHING_WENT_WRONG
                   
                   if let msg = responseData.message {
                       message = msg
                   }
                   failure(message)
               } else {
                   guard let responseData = try? JSONDecoder().decode(LoginFailure.self, from: response.data!) else {
                       failure(self.RESPONSE_ERROR_MSG)
                       return
                   }
                   var message = self.SOMETHING_WENT_WRONG
                   
                   if let msg = responseData.message {
                       message = msg
                   }
                   failure(message)
               }
           }) { (msg) in
               failure(msg)
           }
       }
    func getAboutUsPage(url:String , complition: @escaping (AboutUsModelHomeData) -> Void, failure: @escaping (String) -> Void) {
        ApiClient.shared.callGetUrl(urlString: url, complition: { (response) in
         
            if(response.response?.statusCode == 200) {
                
                guard let responseData = try? JSONDecoder().decode(AboutUsModelHomeData.self, from: response.data!) else {
                    failure(self.RESPONSE_ERROR_MSG)
                    return
                }
                complition(responseData)
            } else {
                guard let responseData = try? JSONDecoder().decode(LoginFailure.self, from: response.data!) else {
                    failure(self.RESPONSE_ERROR_MSG)
                    return
                }
                var message = self.SOMETHING_WENT_WRONG
                
                if let msg = responseData.message {
                    message = msg
                }
                failure(message)
            }
        }) { (msg) in
            failure(msg)
        }
    }
    
    func getNewsDetailsPage(url:String , complition: @escaping (newsDetailsModelHomeData) -> Void, failure: @escaping (String) -> Void) {
        ApiClient.shared.callGetUrl(urlString: url, complition: { (response) in
         
            if(response.response?.statusCode == 200) {
                
                guard let responseData = try? JSONDecoder().decode(newsDetailsModelHomeData.self, from: response.data!) else {
                    failure(self.RESPONSE_ERROR_MSG)
                    return
                }
                complition(responseData)
            } else {
                guard let responseData = try? JSONDecoder().decode(LoginFailure.self, from: response.data!) else {
                    failure(self.RESPONSE_ERROR_MSG)
                    return
                }
                var message = self.SOMETHING_WENT_WRONG
                
                if let msg = responseData.message {
                    message = msg
                }
                failure(message)
            }
        }) { (msg) in
            failure(msg)
        }
    }
    
    func getCategory16(url:String , complition: @escaping (Category16ModelData) -> Void, failure: @escaping (String) -> Void) {
        ApiClient.shared.callGetUrl(urlString: url, complition: { (response) in
         
            if(response.response?.statusCode == 200) {
                
                guard let responseData = try? JSONDecoder().decode(Category16ModelData.self, from: response.data!) else {
                    failure(self.RESPONSE_ERROR_MSG)
                    return
                }
                complition(responseData)
            } else {
                guard let responseData = try? JSONDecoder().decode(LoginFailure.self, from: response.data!) else {
                    failure(self.RESPONSE_ERROR_MSG)
                    return
                }
                var message = self.SOMETHING_WENT_WRONG
                
                if let msg = responseData.message {
                    message = msg
                }
                failure(message)
            }
        }) { (msg) in
            failure(msg)
        }
    }
    
//     func getListOfArticleCategory(url:String, parameter: Parameters, complition: @escaping([AllCategory])-> Void, failure: @escaping (String) -> Void) {
//
//         ApiClient.shared.callPostUrl(urlString: url, param: parameter, complition: { (response) in
//             if(response.response?.statusCode == 200) {
//                 guard let responseData = try? JSONDecoder().decode(ArticleCategory.self, from: response.data!) else {
//                     failure(self.RESPONSE_ERROR_MSG)
//                     return
//                 }
//                 complition(responseData.data.categories)
//
//             } else {
//                 guard let responseData = try? JSONDecoder().decode(ApiFailure.self, from: response.data!) else {
//                     failure(self.RESPONSE_ERROR_MSG)
//                     return
//                 }
//                 var message = self.SOMETHING_WENT_WRONG
//                 if let msg = responseData.message {
//                     message = msg
//                 } else if let msg = responseData.error {
//                     message = msg
//                 }
//                 failure(message)
//             }
//         }) { (msg) in
//             failure(msg)
//         }
//     }

}
