//
//  ModelClass.swift
//  Mujeeb
//
//  Created by iroid on 05/11/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import Foundation
struct CategoryData:Codable {
    var id: String?
    var taxi_category: String?
    var icon_path: String?
}
struct InnerCategoryData:Codable {
    var success: Bool?
    var result: [InnerData]?
}
struct InnerData:Codable {
    var main_cat_id: String?
    var minor_cat_id: String?
    var sub_cat_id: String?
    var minor_cat_name: String?
    var icon_path: String?
    var sub_cat_name:String?
    var dt_created:String?
    var has_minor:String?
    var contact_number1:String?
    var contact_number2:String?
    var institution_name:String?
    var description:String?
    var Address:String?
    var taxi_name:String?
    var driver_name:String?
    var driver_address:String?
    var vehicle_name:String?
    var on_time:String?
    var Ariyipp:String?
    var vehicle_number:String?
    var service_url:String?
    var name: String?
    var house_name: String?
    var ward_no: String?
    var door_no: String?
    var BloodGroup: String?
    var author:String?
     var Reg_Number:String?
    var book_name:String?
    var fare:String?
    var contact_person:String?
    var image_path: String?
    var file_path:String?
}

struct NewsModelHomeData:Codable {
    var success: Bool?
    var result: [NewsModelHome]?
}

struct NewsModelHome : Codable {
    var main_cat_id : String!
    var sub_cat_id : String!
    var Newsheading : String!
    var scroll_type : String!
    var service_url : String!
}

struct BannerModelHomeData:Codable {
    var success: Bool?
    var result: [BannerModelHome]?
}

struct BannerModelHome : Codable {
    var image_path : String!
    var service_url : String!
}

struct AboutUsModelHomeData:Codable {
    var success: Bool?
    var result: [AboutUsModelHome]?
}

struct AboutUsModelHome : Codable {
    var image_path : String!
    var service_url : String!
    var details:String?
    var contact_number1:String?
    var history:String?
}

struct newsDetailsModelHomeData:Codable {
    var success: Bool?
    var result: [newsDetailsModelHome]?
}

struct newsDetailsModelHome : Codable {
    var para_heading : String!
    var image_path : String!
    var detailedNews : String!
}

struct Category16ModelData:Codable {
    var success: Bool?
    var result: [Category16Model]?
}

struct Category16Model : Codable {
    var main_cat_id = String()
    var sub_cat_id = String()
    var sub_cat_name = String()
    var service_url = String()
}
struct Success:Codable {
    var success: Bool?
    var result: String?
}
