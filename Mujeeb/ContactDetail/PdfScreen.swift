//
//  PdfScreen.swift
//  Mujeeb
//
//  Created by iroid on 01/02/21.
//  Copyright © 2021 iroid. All rights reserved.
//

import UIKit

class PdfScreen: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var noDataFoundView: UIView!
//    @IBOutlet weak var pdfCollectionView: UICollectionView!
    @IBOutlet weak var pdfTableView: UITableView!
    var diffcategoryArray:[InnerData] = []
    var categoryId = ""
    var titleLabelText = ""
    var imageString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialDetail()
    }
    func initialDetail(){
//        let nib = UINib(nibName: "DownloadPDFCell", bundle: nil)
//        pdfTableView.register(nib, forCellReuseIdentifier: "DownloadPDFCell")
        titleLabel.text = titleLabelText
        pdfTableView.tableFooterView = UIView()
        getPdf()
    }

    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onHome(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "HomeScreen") as! HomeScreen
        self.navigationController?.pushViewController(control, animated: false)
    }
  
    func getPdf() {
        let url = "http://pallisseri.com/eps/ios-api/ios/getApplicationDetails.php?main_cat_id=15&sub_cat_id=\(categoryId)"
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getInnerCategory(url: url, complition: { (categoryData) in
            Utility.hideIndicator()
            print(categoryData)
            
            self.diffcategoryArray = categoryData.result!
            if self.diffcategoryArray.count > 0{
                self.pdfTableView.reloadData()
                self.pdfTableView.isHidden = false
                self.noDataFoundView.isHidden = true
            }else{
                self.pdfTableView.isHidden = true
                self.noDataFoundView.isHidden = false
            }
            
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
}
extension PdfScreen:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return diffcategoryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DownloadPDFTableViewCell", for: indexPath)
        let descriptionLabel = cell.viewWithTag(1200) as! UILabel
        let downloadButton = cell.viewWithTag(1201) as! UIButton
        let model = diffcategoryArray[indexPath.row]
        downloadButton.tag = indexPath.row
//        descriptionLabel.text = "slidailsd"
        descriptionLabel.text = model.description
        
        downloadButton.addTarget(self, action: #selector(onDownload(sender:)), for: .touchUpInside)

        return cell
    }
    
    @objc func onDownload(sender: UIButton){
        let buttonTag = sender.tag
        let model = diffcategoryArray[buttonTag]
//        let pdf = model.file_path
        let str = "\(ICON_URL)\(model.file_path ?? "")"
        savePdf(urlString: str, subCatId: model.sub_cat_id ?? "0")
    }
    
    func savePdf(urlString:String, subCatId:String) {
        DispatchQueue.main.async {
            
            let textInput = urlString
            let result = textInput.filter { !$0.isWhitespace }
//            let trimmedString = urlString.trimmingCharacters(in: .whitespaces)
            let url = URL(string: result)
            let pdfData = try? Data.init(contentsOf: url!)
            let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
            
            let pdfNameFromUrl = "Pallisseri_\(subCatId).pdf"
            let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
            do {
                try pdfData?.write(to: actualPath, options: .atomic)
                print("pdf successfully saved!")
//                self.showSavedPdf(url: self.pdfUrl!, fileName: self.MoveInMoveOutString ?? "0")
            } catch {
                print("Pdf could not be saved")
            }
        }
    }
    
    func showSavedPdf(url:String, fileName:String) {
        if #available(iOS 10.0, *) {
            do {
                let docURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                let contents = try FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
                for url in contents {
                    if url.description.contains("\(fileName).pdf") {
                        // its your file! do what you want with it!
                        
                    }
                }
            } catch {
                print("could not locate pdf file !!!!!!!")
            }
        }
    }
}

