//
//  ContactFormScreen.swift
//  Mujeeb
//
//  Created by iroid on 28/12/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class ContactFormScreen: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var contactTextField: UITextField!
    @IBOutlet weak var institutionTextView: UITextView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var addressTextView: UITextView!
    @IBOutlet weak var titleLabel: UILabel!
    var subCatId = "0"
    var minor_cat_id = "0"
    var categoryName = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = categoryName
    }
    

  

    @IBAction func onHome(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "HomeScreen") as! HomeScreen
        self.navigationController?.pushViewController(control, animated: false)
    }
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onSubmit(_ sender: Any) {
        if nameTextField.text?.trimmingCharacters(in: .whitespaces) == ""{
            Utility.showAlert(vc: self, message: "നിങ്ങളുടെ പേര് ടൈപ്പ് ചെയ്യൂ")
            return
        }else if contactTextField.text?.trimmingCharacters(in: .whitespaces) == ""{
            Utility.showAlert(vc: self, message: "മൊബൈല്‍ നമ്പര്‍ ടൈപ്പ് ചെയ്യൂ")
            return
        }else if addressTextView.text?.trimmingCharacters(in: .whitespaces) == ""{
            Utility.showAlert(vc: self, message: "അഡ്രസ്സ് ടൈപ്പ് ചെയ്യൂ")
            return
        }else if institutionTextView.text?.trimmingCharacters(in: .whitespaces) == ""{
            Utility.showAlert(vc: self, message: "ഹെഡിംഗ് ടൈപ്പ് ചെയ്യൂ")
            return
        }else if descriptionTextView.text?.trimmingCharacters(in: .whitespaces) == ""{
            Utility.showAlert(vc: self, message: "കൂടുതല്‍ വിവരം ടൈപ്പ് ചെയ്യൂ")
            return
        }
        getHomeBanner()
    }
    
    
    func getHomeBanner() {
//        let name = nameTextField.text?.trimmingCharacters(in: .whitespaces)
        let nameString  =  nameTextField.text?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let institutionString = institutionTextView.text?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let descriptionString = descriptionTextView.text.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let addressTextString = addressTextView.text.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = BASE_URL + "save_calssified.php?main_cat_id=7&sub_cat_id=" + "\(subCatId)" + "&minor_cat_id=" + "\(minor_cat_id)" + "&contact_person='" + "\(nameString ?? "")" + "'&contact_number=" + "\(contactTextField.text!)" + "&institution_name='" + "\(institutionString ?? "")" + "'&description='" + "\(descriptionString ?? "")" + "'&Address='" + "\(addressTextString ?? "")" + "'"
        
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.addAdv(url: url, param: [:], complition: { [self] (bannerData) in
            Utility.hideIndicator()
            self.navigationController?.popViewController(animated: true)
            
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
    
}
