//
//  ContactDetailScreen.swift
//  Mujeeb
//
//  Created by iroid on 01/12/20.
//  Copyright © 2020 iroid. All rights reserved.
//
//7
import UIKit

class ContactDetailScreen: UIViewController {
    @IBOutlet weak var dataTitleLabel: UILabel!
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var noDataFoundView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!
    @IBOutlet weak var formViewHeight: NSLayoutConstraint!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var searchView: UIView!
    
    var id = "0"
    var minorCatId = "0"
    var hasMinor = "0"
    var innerCategoryArray:[InnerData] = []
    var searchInnerCategoryArray:[InnerData] = []
    var titleString = String()
    var imagePath = ""
    var taxiDetailsId = ""
    var mainCategoryId = 0
//    var imageString = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        dataTitleLabel.text = titleString
        self.noDataFoundView.isHidden = true
        
        if id == "205" || id == "152" || id == "171"{
            if id == "205"{
                self.formViewHeight.constant = 100
                self.formView.isHidden = false
            }else{
                self.formViewHeight.constant = 0
                self.formView.isHidden = true
            }
            self.searchViewHeight.constant  = 56
            self.searchView.isHidden = false
           
        }else{
            self.formViewHeight.constant = 0
            self.searchViewHeight.constant = 0
            self.searchView.isHidden = true
            self.formView.isHidden = true
        }
        if  mainCategoryId == 4 ||  mainCategoryId == 6{
            self.searchViewHeight.constant  = 56
            self.searchView.isHidden = false
        }
        self.initialDetails()
        self.dataTableView.register(UINib(nibName: "ContactUSTableViewCell", bundle: nil), forCellReuseIdentifier: "categoryInnerCell")
        self.searchTextField.addTarget(self, action: #selector(self.changeText(textfield:)), for: .editingChanged)

        // Do any additional setup after loading the view.
    }
    @objc func changeText(textfield: UITextField){
      
            if textfield.text!.trimmingCharacters(in: .whitespacesAndNewlines).count > 0{
                self.searchInnerCategoryArray = innerCategoryArray.filter({$0.institution_name!.lowercased().hasPrefix(textfield.text!.lowercased())})
                self.dataTableView.reloadData()
            }else{
                self.searchInnerCategoryArray = []
                self.dataTableView.reloadData()
            }
    }
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSearch(_ sender: Any) {
    }
    @IBAction func onHome(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "HomeScreen") as! HomeScreen
        self.navigationController?.pushViewController(control, animated: false)
    }
    @IBAction func onForm(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Contact", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "ContactFormScreen") as! ContactFormScreen
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    
    @objc func onCallClick(sender: UIButton){
        if let phoneNumber = self.innerCategoryArray[sender.tag].contact_number1{
            Utility.openDialPad(phone: phoneNumber)
        }
    }
    
    @objc func onWhatsappClick(sender: UIButton){
        if let phoneNumber = self.innerCategoryArray[sender.tag].contact_number1{
            Utility.openChatWhatsapp(phone: phoneNumber)
        }
    }
    
    @objc func onShareClick(sender: UIButton){
//        if let phoneNumber = self.innerCategoryArray[sender.tag].contact_number1{
            Utility.shareMessage(viewController: self, text: "Test Message")
//        }
    }
    
    // MARK: - Initial Details
    func initialDetails() {
        
        if taxiDetailsId != "" {
            getTaxiDetail()
        }else{
            getCategory()
        }
    }
    //MARK:- API METHOD
    func getCategory() {
        let url = BASE_URL +  API.GET_CONTACT_DETAIL + id + "&minor_cat_id=" + hasMinor
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getInnerCategory(url: url, complition: { (categoryData) in
            Utility.hideIndicator()
            print(categoryData)
            self.innerCategoryArray = categoryData.result!
            if self.innerCategoryArray.count > 0{
                self.dataTableView.isHidden = false
                self.noDataFoundView.isHidden  = true
            }else{
                self.dataTableView.isHidden = true
                self.noDataFoundView.isHidden  = false
            }
            self.dataTableView.reloadData()
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
    
    //MARK:- API METHOD
    func getTaxiDetail() {
        let url = BASE_URL +  API.TAXI_DETAILS  + taxiDetailsId
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getInnerCategory(url: url, complition: { (categoryData) in
            Utility.hideIndicator()
            print(categoryData)
            self.innerCategoryArray = categoryData.result!
            if self.innerCategoryArray.count > 0{
                self.dataTableView.isHidden = false
                self.noDataFoundView.isHidden  = true
            }else{
                self.dataTableView.isHidden = true
                self.noDataFoundView.isHidden  = false
            }
            self.dataTableView.reloadData()
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
}

extension ContactDetailScreen : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.searchTextField.text!.count > 0 && self.searchInnerCategoryArray.isEmpty) ? 0 : (self.searchInnerCategoryArray.isEmpty ? innerCategoryArray.count : searchInnerCategoryArray.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.dataTableView.dequeueReusableCell(withIdentifier: "categoryInnerCell", for: indexPath)as! ContactUSTableViewCell
        let model =  self.searchInnerCategoryArray.isEmpty ?  innerCategoryArray[indexPath.row] : searchInnerCategoryArray[indexPath.row]
        cell.phoneNumberButton.tag = indexPath.row
        cell.phoneNumberButton.addTarget(self, action: #selector(self.onCallClick(sender:)), for: .touchUpInside)
        cell.onWhatssapp.tag = indexPath.row
        cell.onWhatssapp.addTarget(self, action: #selector(self.onWhatsappClick(sender:)), for: .touchUpInside)
        cell.onShare.tag = indexPath.row
        cell.onShare.addTarget(self, action: #selector(self.onShareClick(sender:)), for: .touchUpInside)

        if taxiDetailsId != "" {
            cell.titleLabel.text = model.driver_name
            cell.addressLabel.text = model.driver_address
            cell.mobileNumberLabel.text = model.contact_number1
            
            if model.driver_name != ""{
                cell.imageViewHeight.constant = 25
                cell.iconImageView.isHidden = false
                cell.titleLabel.isHidden = false
                cell.lineLabel.isHidden = false
            }else{
                cell.imageViewHeight.constant = 0
                cell.iconImageView.isHidden = true
                cell.titleLabel.isHidden = true
                cell.lineLabel.isHidden = true
            }
            if model.taxi_name != ""{
                cell.infoView.isHidden = false
            }else{
                cell.infoView.isHidden = true
                cell.infoViewHeight.constant = 0
            }
            if model.driver_address != ""{
                cell.locationView.isHidden = false
                
            }else{
                cell.locationView.isHidden = true
                cell.locationViewHeight.constant = 0
            }
            if model.contact_number2 != ""{
                cell.secondMobileNumLabel.text = model.contact_number1
                cell.secoundMobileNumberView.isHidden = false
            }else{
                cell.secoundMobileNumberView.isHidden = true
                cell.secondMobileViewHeight.constant = 0
            }
            cell.infoLAbel.text = model.taxi_name
            
            if model.icon_path != nil && model.icon_path != ""  {
                 let str = "\(ICON_URL)\(model.icon_path ?? "")"
                Utility.setImage(str, imageView: cell.iconImageView)
            }else{
                cell.iconImageView.image = UIImage(named: imagePath)
//                Utility.setImage(imagePath, imageView: cell.iconImageView)
            }
        }else{
            cell.titleLabel.text = model.institution_name
            cell.addressLabel.text = model.Address
            cell.mobileNumberLabel.text = model.contact_number1
            
            
            if model.institution_name != ""{
                cell.imageViewHeight.constant = 25
                cell.iconImageView.isHidden = false
                cell.titleLabel.isHidden = false
                cell.lineLabel.isHidden = false
            }else{
                cell.imageViewHeight.constant = 0
                cell.iconImageView.isHidden = true
                cell.titleLabel.isHidden = true
                cell.lineLabel.isHidden = true
            }
            if model.description != ""{
                cell.infoView.isHidden = false
            }else{
                cell.infoView.isHidden = true
                cell.infoViewHeight.constant = 0
            }
            
            if model.Address != ""{
                cell.locationView.isHidden = false
                
            }else{
                cell.locationView.isHidden = true
                cell.locationViewHeight.constant = 0
            }
            if model.contact_number2 != ""{
                cell.secondMobileNumLabel.text = model.contact_number1
                cell.secoundMobileNumberView.isHidden = false
            }else{
                cell.secoundMobileNumberView.isHidden = true
                cell.secondMobileViewHeight.constant = 0
            }
            cell.infoLAbel.text = model.description
            
            if model.icon_path != nil && model.icon_path != ""  {
                let str = "\(ICON_URL)\(model.icon_path ?? "")"
                Utility.setImage(str, imageView: cell.iconImageView)
            }else{
                Utility.setImage(imagePath, imageView: cell.iconImageView)
            }
        }
        return cell
    }
}
