//
//  SoundContactScreen.swift
//  Mujeeb
//
//  Created by iroid on 03/12/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class SoundContactScreen: UIViewController {
    @IBOutlet weak var dataTitleLabel: UILabel!
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var noDataFoundView: UIView!
    
    var id = "0"
    var minorCatId = "0"
    var hasMinor = "0"
    var innerCategoryArray:[InnerData] = []
    var titleString = String()
    var imagePath = ""
    var taxiDetailsId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        dataTitleLabel.text = titleString
        self.noDataFoundView.isHidden = true
        self.initialDetails()
        self.dataTableView.register(UINib(nibName: "SoundDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "SoundCell")
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onHome(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "HomeScreen") as! HomeScreen
        self.navigationController?.pushViewController(control, animated: false)
    }
    
    // MARK: - Initial Details
    func initialDetails() {
        
        if taxiDetailsId != "" {
            getTaxiDetail()
        }else{
            getCategory()
        }
    }
    //MARK:- API METHOD
    func getCategory() {
        let url = BASE_URL +  API.GET_NOTICE_DETAILS + id + "&sub_cat_id=" + hasMinor
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getInnerCategory(url: url, complition: { (categoryData) in
            Utility.hideIndicator()
            print(categoryData)
            self.innerCategoryArray = categoryData.result!
            if self.innerCategoryArray.count > 0{
                self.dataTableView.isHidden = false
                self.noDataFoundView.isHidden  = true
            }else{
                self.dataTableView.isHidden = true
                self.noDataFoundView.isHidden  = false
            }
            self.dataTableView.reloadData()
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
    
    //MARK:- API METHOD
    func getTaxiDetail() {
        let url = BASE_URL +  API.TAXI_DETAILS  + taxiDetailsId
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getInnerCategory(url: url, complition: { (categoryData) in
            Utility.hideIndicator()
            print(categoryData)
            self.innerCategoryArray = categoryData.result!
            if self.innerCategoryArray.count > 0{
                self.dataTableView.isHidden = false
                self.noDataFoundView.isHidden  = true
            }else{
                self.dataTableView.isHidden = true
                self.noDataFoundView.isHidden  = false
            }
            self.dataTableView.reloadData()
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
}
extension SoundContactScreen : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return innerCategoryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.dataTableView.dequeueReusableCell(withIdentifier: "SoundCell", for: indexPath)as! SoundDetailTableViewCell
        let model = innerCategoryArray[indexPath.row]
        
        let dateFormatter = DateFormatter()
               dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
               let date = dateFormatter.date(from: model.dt_created ?? "")

               dateFormatter.dateFormat = "dd/MM/yyyy"
               let goodDate = dateFormatter.string(from: date!)
               cell.dateLabel.text = goodDate
        
//        cell.dateLabel.text = model.dt_created
        cell.detailLabel.text = model.Ariyipp
        return cell
    }
    
    
}

