//
//  InnerDataScreen.swift
//  Mujeeb
//
//  Created by iroid on 03/11/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class InnerDataScreen: UIViewController {

    @IBOutlet weak var dataTitleLabel: UILabel!
    @IBOutlet weak var dataTableView: UITableView!
    var dataArray:[HomeCategoryModel] = []
    var id = "0"
    var minorCatId = "0"
    var hasMinor = "0"
    var innerCategoryArray:[InnerData] = []
    var titleString = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataTitleLabel.text = titleString
        self.initialDetails()
        self.dataTableView.register(UINib(nibName: "CategoryInnerDataTableViewCell", bundle: nil), forCellReuseIdentifier: "categoryInnerCell")
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onHome(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "HomeScreen") as! HomeScreen
        self.navigationController?.pushViewController(control, animated: false)
    }
    
    // MARK: - Initial Details
    func initialDetails() {
        dataArray = [HomeCategoryModel(imageName: "taxi_small", name: "Car/Bus"),
                         HomeCategoryModel(imageName: "taxi_small", name: "Car/Bus"),
                         HomeCategoryModel(imageName: "taxi_small", name: "Car/Bus"),
                         HomeCategoryModel(imageName: "taxi_small", name: "Car/Bus"),
                         HomeCategoryModel(imageName: "taxi_small", name: "Car/Bus"),
                         HomeCategoryModel(imageName: "taxi_small", name: "Car/Bus")]
        getCategory()
    }
    
    //MARK:- API METHOD
    func getCategory() {
       
        let url = BASE_URL +  API.GET_CONTACT_DETAIL + id + "&minor_cat_id=" + hasMinor
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getInnerCategory(url: url, complition: { (categoryData) in
            Utility.hideIndicator()
            print(categoryData)
            self.innerCategoryArray = categoryData.result!
            self.dataTableView.reloadData()
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
}

extension InnerDataScreen : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return innerCategoryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.dataTableView.dequeueReusableCell(withIdentifier: "categoryInnerCell", for: indexPath)as! CategoryInnerDataTableViewCell
        let model = innerCategoryArray[indexPath.row]
        cell.categoryNameLabel.text = model.minor_cat_name
        cell.mobileNumberLabel.text = model.contact_number1
        let str = "\(ICON_URL)\(model.icon_path ?? "")"
        Utility.setImage(str, imageView: cell.categoryImageView)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}
