//
//  SondScreen.swift
//  Mujeeb
//
//  Created by iroid on 05/11/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class SoundScreen: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var soundTableView: UITableView!
    
    var categoryArray:[InnerData] = []
    var categoryId = 0
    var titleText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = titleText
        initialDetails()
    }
    
    // MARK: - Initial Details
    func initialDetails() {
        self.soundTableView.register(UINib(nibName: "SoundTableViewCell", bundle: nil), forCellReuseIdentifier: "SoundCell")
        anotherCategory(id: categoryId)
    }
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onHome(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "HomeScreen") as! HomeScreen
        self.navigationController?.pushViewController(control, animated: false)
    }
    
    //MARK:- API METHOD
    func anotherCategory(id:Int) {
        print(id)
        let url = BASE_URL +  API.MAIN_CATEGORY + "\(id)"
        Utility.showIndecator()
        CategoryApiManager.shared.getInnerCategory(url: url, complition: { (categoryData) in
            Utility.hideIndicator()
            print(categoryData)
            self.categoryArray = categoryData.result!
            self.soundTableView.reloadData()
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
}
extension SoundScreen: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SoundCell") as! SoundTableViewCell
        let model = categoryArray[indexPath.row]
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: model.dt_created ?? "")
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let goodDate = dateFormatter.string(from: date!)
        cell.descriptionLabel.text = model.sub_cat_name
        cell.postedLabel.text = goodDate
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = categoryArray[indexPath.row]
        let storyBoard = UIStoryboard(name: "Contact", bundle: nil)
        let str = "13"
        let control = storyBoard.instantiateViewController(withIdentifier: "SoundContactScreen") as! SoundContactScreen
        control.id = String(categoryId)
        control.hasMinor = model.sub_cat_id!
        control.title = model.sub_cat_name
        control.imagePath = str
        self.navigationController?.pushViewController(control, animated: true)
    }
    
}
