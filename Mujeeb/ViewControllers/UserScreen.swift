//
//  UserScreen.swift
//  Mujeeb
//
//  Created by Kishan on 08/11/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class UserScreen: UIViewController {
    var aboutUsArray = [AboutUsModelHome]()

    @IBOutlet weak var mobileNumLabel: UILabel!
    @IBOutlet weak var userInFoLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        appInFo()
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onHome(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "HomeScreen") as! HomeScreen
        self.navigationController?.pushViewController(control, animated: false)
    }

    @IBAction func onCall(_ sender: Any) {}
    
    @IBAction func onWhatsApp(_ sender: Any) {}
    
    @IBAction func onShare(_ sender: Any) {}
    
    //MARK: API
    func appInFo() {
        let url = "https://pallisseri.com/eps/ios-api/ios/getAdminDetails.php?main_cat_id=19"
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getAboutUsPage(url: url, complition: { (newsData) in
            Utility.hideIndicator()
            print("About Us ->",newsData)
            self.aboutUsArray = newsData.result!
            self.userInFoLabel.text = self.aboutUsArray[0].details
            self.mobileNumLabel.text = self.aboutUsArray[0].contact_number1
            //self.dataTableView.reloadData()
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
}
