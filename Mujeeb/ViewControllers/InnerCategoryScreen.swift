//
//  InnerCategoryScreen.swift
//  Mujeeb
//
//  Created by Kishan on 02/11/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit
class InnerCategoryScreen: UIViewController {
    
    @IBOutlet weak var catogoryTitleLabel: UILabel!
    @IBOutlet weak var innerCategoryCollectionView: UICollectionView!
    @IBOutlet weak var slideCollectionView: UICollectionView!
    @IBOutlet weak var sliderCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var sliderViewheight: NSLayoutConstraint! //50
    @IBOutlet weak var hideShowButtonHeight: NSLayoutConstraint! // 40
    @IBOutlet weak var sliderButton: dateSportButton!
    @IBOutlet weak var switcher: UISwitch!
    @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var imgeView: UIImageView!
    
    var categoryArray:[CategoryData] = []
    var diffcategoryArray:[InnerData] = []
    var category16Array = [Category16Model]()
    var bloodArray: [HomeCategoryModel] = []
    var categoryId = 0
    var titleLabel = ""
    var imageString = ""
    var advUrl = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialDetails()
        innerCategoryCollectionView.register(UINib(nibName: "InnerCategoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "innerCategoryCell")
        innerCategoryCollectionView.register(UINib(nibName: "BloodCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BloodCollectionViewCell")
        
        slideCollectionView.register(UINib(nibName: "SlideCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SlideCell")
        
        catogoryTitleLabel.text = titleLabel
        
        self.sliderViewheight.constant = categoryId == 12 ? 50 : 0
        self.hideShowButtonHeight.constant = 0
        self.switcher.isHidden = categoryId != 12
        self.sliderButton.isHidden = true
        self.switcher.addTarget(self, action: #selector(self.valueChanged(sender:)), for: .valueChanged)
        self.imageViewHeight.constant = 0//categoryId == 4 ? 100 : 0
    }
    
    @objc func valueChanged(sender: UISlider){
        if self.switcher.isOn{
            self.hideShowButtonHeight.constant = 40
            self.sliderButton.isHidden = false
        }else{
            self.sliderButton.isHidden = true
            self.hideShowButtonHeight.constant = 0
        }
    }
    @IBAction func onWhatsapp(_ sender: Any) {
        self.showWhatsAppNum()
    }
    
    func setArray(){
        bloodArray = [
            HomeCategoryModel(imageName: "blood_positive", name: "A +ve"),
            HomeCategoryModel(imageName: "blood_negative", name: "A -ve "),
            HomeCategoryModel(imageName: "blood_positive", name: "B +ve"),
            HomeCategoryModel(imageName: "blood_negative", name: "B -ve "),
            HomeCategoryModel(imageName: "blood_positive", name: "O +ve"),
            HomeCategoryModel(imageName: "blood_negative", name: "0 -ve "),
            HomeCategoryModel(imageName: "blood_positive", name: "AB +ve"),
            HomeCategoryModel(imageName: "blood_negative", name: "AB -ve"),
            HomeCategoryModel(imageName: "blood_negative", name: "Other"),
        ]
    }
    
    // MARK: - Button Click
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onHome(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "HomeScreen") as! HomeScreen
        self.navigationController?.pushViewController(control, animated: false)
    }
    
    
    
    @IBAction func onAdv(_ sender: UIButton) {
        
        guard let url = URL(string: advUrl) else { return }
        UIApplication.shared.open(url)
    }
    
    // MARK: - Initial Details
    func initialDetails() {
        
        if categoryId == 0{
            getCategory()
        }
        else if categoryId == 16{
            self.getCategory16()
        }else if categoryId == 18{
            self.getCategory18()
        }else if categoryId == 12{
//            self.getCategory12()
            pageController.isHidden = false
            sliderCollectionViewHeight.constant = 200
            self.setArray()
        }
        else{
            anotherCategory(id: categoryId)
        }
//        if categoryId == 4{
            self.showImage()
//        }
    }
}

extension InnerCategoryScreen : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == slideCollectionView {
            return 1
        }else{
        if categoryId == 0{
            return categoryArray.count
        }
        else if categoryId == 16 {
            return category16Array.count
        }else if categoryId == 12{
            return bloodArray.count
        }else{
            if self.categoryId == 11{
                return  self.diffcategoryArray.isEmpty ? 0 : diffcategoryArray.count + 1
            }else{
                return diffcategoryArray.count
            }
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == slideCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SlideCell", for: indexPath) as! SlideCollectionViewCell
            // let model = slideArray[indexPath.row]
            // Utility.setImage("\(ICON_URL)\(model.image_path ?? "")", imageView: cell.slideImageView)
            cell.slideImageView.image = UIImage(named: "blood_banner")
            return cell
        }else{
            if categoryId == 12{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BloodCollectionViewCell", for: indexPath) as! BloodCollectionViewCell
                cell.imgView.image = UIImage(named: self.bloodArray[indexPath.row].imageName ?? "")
                cell.titleLabel.text = self.bloodArray[indexPath.row].name
                return cell
            }
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "innerCategoryCell", for: indexPath) as! InnerCategoryCollectionViewCell
            if categoryId == 0{
                let model = categoryArray[indexPath.row]
                cell.categoryLabel.text = model.taxi_category
                let str = "\(ICON_URL)\(model.icon_path ?? "")"
                if model.icon_path == ""{
                    cell.categoryImageView.image = UIImage(named: imageString)
                }else{
                    Utility.setImage(str, imageView: cell.categoryImageView)
                }
                
                
            }
            else if categoryId == 16 {
                let model = category16Array[indexPath.row]
                cell.categoryLabel.text = model.sub_cat_name
            }   else if categoryId == 18 {
                let model = diffcategoryArray[indexPath.row]
                cell.categoryLabel.text = model.sub_cat_name
                cell.categoryImageView.contentMode = .scaleAspectFit
                cell.categoryImageView.image = UIImage(named: imageString)
                
            }
            else{
                if categoryId == 11{
                    if indexPath.row == 0{
                        let model = diffcategoryArray[indexPath.row]
                        cell.categoryLabel.text = model.sub_cat_name
                        let str = "\(ICON_URL)\(model.icon_path ?? "")"
                        if model.icon_path == ""{
                            cell.categoryImageView.contentMode = .scaleAspectFit
                            cell.categoryImageView.image = UIImage(named: imageString)
                        }else{
                            Utility.setImage(str, imageView: cell.categoryImageView)
                        }
                    }else{
                        let model = diffcategoryArray[0]
                        cell.categoryLabel.text = "റജിസ്റ്റര്‍ ചെയ്യുക"
                        let str = "\(ICON_URL)\(model.icon_path ?? "")"
                        if model.icon_path == ""{
                            cell.categoryImageView.contentMode = .scaleAspectFit
                            cell.categoryImageView.image = UIImage(named: "edit_icon")
                        }else{
                            cell.categoryImageView.image = UIImage(named: "edit_icon")
//                            Utility.setImage(str, imageView: "library")
                        }
                    }
                }else{
                    let model = diffcategoryArray[indexPath.row]
                    cell.categoryLabel.text = model.sub_cat_name
                    let str = "\(ICON_URL)\(model.icon_path ?? "")"
                    if model.icon_path == ""{
                        cell.categoryImageView.contentMode = .scaleAspectFit
                        cell.categoryImageView.image = UIImage(named: imageString)
                    }else{
                        Utility.setImage(str, imageView: cell.categoryImageView)
                    }
                }
                //            Utility.setImage(str, imageView: cell.categoryImageView)
            }
            return cell
        }
        //return UICollectionViewCell()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView != slideCollectionView{
          if categoryId == 0{
            let model = categoryArray[indexPath.row]
            //            let storyBoard = UIStoryboard(name: "Home", bundle: nil)
            //            let control = storyBoard.instantiateViewController(withIdentifier: "InnerDataScreen") as! InnerDataScreen
            //            control.id = model.id!
            //            control.title = model.taxi_category
            //            self.navigationController?.pushViewController(control, animated: true)
            let str = "\(ICON_URL)\(model.icon_path ?? "")"
            let storyBoard = UIStoryboard(name: "Contact", bundle: nil)
            let control = storyBoard.instantiateViewController(withIdentifier: "ContactDetailScreen") as! ContactDetailScreen
            control.taxiDetailsId = model.id!
            control.title = model.taxi_category
            control.imagePath = str
            control.hasMinor = "1"
            control.imagePath = imageString
            self.navigationController?.pushViewController(control, animated: true)
        }else if categoryId == 9{
            let model = diffcategoryArray[indexPath.row]
            let storyBoard = UIStoryboard(name: "Contact", bundle: nil)
            let str = "bus_icon"
            let control = storyBoard.instantiateViewController(withIdentifier: "BusScreen") as! BusScreen
            control.id = String(categoryId)
            control.hasMinor = model.sub_cat_id!
            control.title = model.sub_cat_name
            control.imagePath = str
            self.navigationController?.pushViewController(control, animated: true)
            
        }else if categoryId == 10{
            let model = diffcategoryArray[indexPath.row]
            let storyBoard = UIStoryboard(name: "Contact", bundle: nil)
            let str = "10"
            let control = storyBoard.instantiateViewController(withIdentifier: "TrainDetailScreen") as! TrainDetailScreen
            control.id = String(categoryId)
            control.hasMinor = model.sub_cat_id!
            control.title = model.sub_cat_name
            control.imagePath = str
            self.navigationController?.pushViewController(control, animated: true)
        }
        else if categoryId == 13{
            let model = diffcategoryArray[indexPath.row]
            let storyBoard = UIStoryboard(name: "Contact", bundle: nil)
            let str = "bus_icon"
            let control = storyBoard.instantiateViewController(withIdentifier: "SoundContactScreen") as! SoundContactScreen
            control.id = String(categoryId)
            control.hasMinor = model.sub_cat_id!
            control.title = model.sub_cat_name
            control.imagePath = str
            self.navigationController?.pushViewController(control, animated: true)
            
        }  else if categoryId == 14{
            let model = diffcategoryArray[indexPath.row]
            
            let storyBoard = UIStoryboard(name: "Search", bundle: nil)
            let control = storyBoard.instantiateViewController(withIdentifier: "BookDetailScreen") as! BookDetailScreen
            control.innerData = model
            control.titleText = model.sub_cat_name ?? ""
            self.navigationController?.pushViewController(control, animated: true)
            
        }else if categoryId == 15{
            let model = diffcategoryArray[indexPath.row]
            let storyBoard = UIStoryboard(name: "Contact", bundle: nil)
            let control = storyBoard.instantiateViewController(withIdentifier: "PdfScreen") as! PdfScreen
//            control.innerData = model
            control.categoryId = model.sub_cat_id ?? ""
            control.titleLabelText = model.sub_cat_name ?? ""
            self.navigationController?.pushViewController(control, animated: true)
        }else if categoryId == 16 {
            let model = category16Array[indexPath.row]
            if model.service_url != "" {
                guard let url = URL(string: model.service_url) else { return }
                UIApplication.shared.open(url)
            }
        }
        else if categoryId == 17{
            let model = diffcategoryArray[indexPath.row]
            let storyBoard = UIStoryboard(name: "Home", bundle: nil)
            let control = storyBoard.instantiateViewController(withIdentifier: "MenuFormDetailScreen") as! MenuFormDetailScreen
            control.id = String(categoryId)
            control.hasMinor = model.sub_cat_id!
            control.titleStr = model.sub_cat_name ?? ""
            self.navigationController?.pushViewController(control, animated: true)
            
        }
        
        else if categoryId == 18 {
            let model = diffcategoryArray[indexPath.row]
            if model.service_url != "" {
                guard let url = URL(string: model.service_url ?? "") else { return }
                UIApplication.shared.open(url)
            }
            
        }else if categoryId == 11 {
            if indexPath.row == 0{
                let model = diffcategoryArray[indexPath.row]
                let str = "\(ICON_URL)\(model.icon_path ?? "")"
                let storyBoard = UIStoryboard(name: "Home", bundle: nil)
                let control = storyBoard.instantiateViewController(withIdentifier: "MinorInnerScreen") as! MinorInnerScreen
                control.categoryId = model.sub_cat_id!
                control.titleLabel = model.sub_cat_name ?? ""
                self.navigationController?.pushViewController(control, animated: true)
            }else if indexPath.row == 1{
                //MARK:- Edit click
                showWhatsAppNum()
            }
        }else if categoryId == 12{
            let storyBoard = UIStoryboard(name: "Home", bundle: nil)
            let control = storyBoard.instantiateViewController(withIdentifier: "MinorInnerScreen") as! MinorInnerScreen
            control.categoryId = "12"
//            control.titleLabel = model.sub_cat_name ?? ""
            switch indexPath.row{
            case 0:
                control.bloodGroup = "A+"
                break
            case 1:
                control.bloodGroup = "A-"
                break
            case 2:
                control.bloodGroup = "B+"
                break
            case 3:
                control.bloodGroup = "B-"
                break
            case 4:
                control.bloodGroup = "O+"
                break
            case 5:
                control.bloodGroup = "O-"
                break
            case 6:
                control.bloodGroup = "AB+"
                break
            case 7:
                control.bloodGroup = "AB-"
                break
            default:
                control.bloodGroup = "Other"
            }
            self.navigationController?.pushViewController(control, animated: true)
        }else if categoryId == 7{
            let model = diffcategoryArray[indexPath.row]
            let str = "\(ICON_URL)\(model.icon_path ?? "")"
            let storyBoard = UIStoryboard(name: "Home", bundle: nil)
            if  model.has_minor == "2"{
                let control = storyBoard.instantiateViewController(withIdentifier: "AdScreen") as! AdScreen
                control.id = model.sub_cat_id!
                control.titleString = model.sub_cat_name ?? ""
                control.imagePath = str
                control.hasMinor = "1"
                self.navigationController?.pushViewController(control, animated: true)
             }
        }
        else{
            let model = diffcategoryArray[indexPath.row]
            let str = "\(ICON_URL)\(model.icon_path ?? "")"
            let storyBoard = UIStoryboard(name: "Contact", bundle: nil)
            if  model.has_minor == "2"{
                let control = storyBoard.instantiateViewController(withIdentifier: "ContactDetailScreen") as! ContactDetailScreen
                control.id = model.sub_cat_id!
                control.title = model.sub_cat_name
                control.imagePath = str
                control.hasMinor = "1"
                self.navigationController?.pushViewController(control, animated: true)
                
            }else{
//                let storyBoard = UIStoryboard(name: "Home", bundle: nil)
//                let control = storyBoard.instantiateViewController(withIdentifier: "MinorInnerScreen") as! MinorInnerScreen
//                control.categoryId = model.sub_cat_id!
//                control.titleLabel = model.sub_cat_name ?? ""
//                self.navigationController?.pushViewController(control, animated: true)
                                if categoryId == 4 || categoryId == 6{
                                    let storyBoard = UIStoryboard(name: "Home", bundle: nil)
                                    let control = storyBoard.instantiateViewController(withIdentifier: "MinorInnerScreen") as! MinorInnerScreen
                                    control.categoryId = model.sub_cat_id!
                                    control.titleLabel = model.sub_cat_name ?? ""
                                    control.isSearchShow = false
                                    control.mainCategoryId = categoryId
                                    self.navigationController?.pushViewController(control, animated: true)
                                }else{
                                     let storyBoard = UIStoryboard(name: "Home", bundle: nil)
                                    let control = storyBoard.instantiateViewController(withIdentifier: "InnerDataScreen") as! InnerDataScreen
                                    control.id = model.sub_cat_id!
                                    control.title = model.sub_cat_name
                                    control.hasMinor = model.has_minor ?? ""
                                    self.navigationController?.pushViewController(control, animated: true)
                                }
            }
             
        }
      }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == slideCollectionView{
            return CGSize(width: (UIScreen.main.bounds.width), height: collectionView.frame.height)
        }else{
            if categoryId == 0{
                if indexPath.row == categoryArray.count - 1 {
                    if categoryArray.count  % 2 != 0 {
                        let width = (UIScreen.main.bounds.width)
                        return CGSize(width: width, height: 90)
                    }else{
                        let width = (UIScreen.main.bounds.width / 2)
                        return CGSize(width: width, height: 90)
                    }
                }else{
                    let width = (UIScreen.main.bounds.width / 2)
                    return CGSize(width: width, height: 90)
                }
                
            }else if categoryId == 12{
                if indexPath.row == self.bloodArray.count - 1 {
                    if self.bloodArray.count  % 2 != 0 {
                        let width = (UIScreen.main.bounds.width)
                        return CGSize(width: width, height: 50)
                    }else{
                        let width = (UIScreen.main.bounds.width / 2)
                        return CGSize(width: width, height: 50)
                    }
                }else{
                    let width = (UIScreen.main.bounds.width / 2)
                    return CGSize(width: width, height: 50)
                }
                
                
            }else{
                if categoryId == 11{
                    let width = (UIScreen.main.bounds.width)
                    return CGSize(width: width, height: 90)
                }else{
                    if indexPath.row == diffcategoryArray.count - 1 {
                        if diffcategoryArray.count  % 2 != 0 {
                            let width = (UIScreen.main.bounds.width)
                            return CGSize(width: width, height: 90)
                        }else{
                            let width = (UIScreen.main.bounds.width / 2)
                            return CGSize(width: width, height: 120)
                        }
                    }else{
                        let width = (UIScreen.main.bounds.width / 2)
                        return CGSize(width: width, height: 120)
                    }
            }
                
                //            if (indexPath.row == diffcategoryArray.count - 1 ) { //it's your last cell
                //                  //Load more data & reload your collection view
                //                let isEven = indexPath.row % 2 == 0
                //                if isEditing{
                //
                //                }else{
                //
                //                }
                //
                //                }
            }
        }
    }
    
    //MARK:- API METHOD
    func getCategory() {
        let url = BASE_URL +  API.GET_TAXI_CATEGORY
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getCategory(url: url, complition: { (categoryData) in
            Utility.hideIndicator()
            print(categoryData)
            self.categoryArray = categoryData
            self.innerCategoryCollectionView.reloadData()
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
    
    func showWhatsAppNum() {
        let url = "https://pallisseri.com/eps/ios-api/ios/getDirectoryContactNumber.php"
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getInnerCategory(url: url, complition: { (newsData) in
            Utility.hideIndicator()
            print("ContactNumber",newsData)
//            self.navigationController?.popViewController(animated: true)
            //self.dataTableView.reloadData()
           
            if let phoneNumber =  newsData.result?[0].contact_number1{
                Utility.openChatWhatsapp(phone: phoneNumber)
            }
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
    
    //MARK:- SET IMAGE
    func showImage() {
        let url = "https://pallisseri.com/eps/ios-api/ios/getAdboxSubPage.php?main_cat_id=\(categoryId)&page_id=2"
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getInnerCategory(url: url, complition: { (newsData) in
            Utility.hideIndicator()
            print("ContactNumber",newsData)
//            self.navigationController?.popViewController(animated: true)
            //self.dataTableView.reloadData()
           
            if let imagePath =  newsData.result?.first?.image_path{
                let str = "\(ICON_URL)\(imagePath)"
                self.imgeView.image = UIImage.gif(url: str)
                self.advUrl = newsData.result?.first?.service_url ?? ""
                self.imageViewHeight.constant = 100
            }else{
                self.imageViewHeight.constant = 0
            }
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
    
    func getCategory16() {
        let url = "https://pallisseri.com/eps/ios-api/ios/getEServiceDetails.php?main_cat_id=16"
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getCategory16(url: url, complition: { (categoryData) in
            Utility.hideIndicator()
            print(categoryData)
            self.category16Array = categoryData.result!
            self.innerCategoryCollectionView.reloadData()
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
    func getCategory18() {
        let url = "https://pallisseri.com/eps/ios-api/ios/ListLiveNewsSubCategory.php?main_cat_id=18"
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getInnerCategory(url: url, complition: { (categoryData) in
            Utility.hideIndicator()
            print(categoryData)
            self.diffcategoryArray = categoryData.result!
            self.innerCategoryCollectionView.reloadData()
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
    
    func getCategory12() {
        let url = "https://pallisseri.com/eps/ios-api/ios/getEServiceDetails.php?main_cat_id=12"
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getInnerCategory(url: url, complition: { (categoryData) in
            Utility.hideIndicator()
            print(categoryData)
            self.diffcategoryArray = categoryData.result!
            self.innerCategoryCollectionView.reloadData()
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
    
    func anotherCategory(id:Int) {
        print(id)
        let url = BASE_URL +  API.SUB_CATEGORY + "\(id)"
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getInnerCategory(url: url, complition: { (categoryData) in
            Utility.hideIndicator()
            print(categoryData)
            self.diffcategoryArray = categoryData.result!
            self.innerCategoryCollectionView.reloadData()
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
    
}
