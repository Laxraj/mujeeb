//
//  HomeScreen.swift
//  Mujeeb
//
//  Created by iroid on 01/11/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit
import SwiftGifOrigin

class HomeScreen: UIViewController {
    
    @IBOutlet weak var topCategoryCollectionView: UICollectionView!
    @IBOutlet weak var secondCategoryCollectionView: UICollectionView!
    @IBOutlet weak var thirdCategoryCollectionView: UICollectionView!
    @IBOutlet weak var fourthCategoryCollectionView: UICollectionView!
    
    @IBOutlet weak var newsCollectionView: UICollectionView!
    @IBOutlet weak var slideCollectionView: UICollectionView!
    @IBOutlet weak var pageControllerDot: UIPageControl!
    @IBOutlet weak var menuScrollView: UIScrollView!
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var liveImageView: UIImageView!
    @IBOutlet weak var flashImageView: UIImageView!
    @IBOutlet weak var advImageView: UIImageView!
    
    var homeTopArray:[HomeCategoryModel] = []
    var slideArray:[BannerModelHome] = []
    var menuArray:[HomeCategoryModel] = []
    var secondCategoryArray:[HomeCategoryModel] = []
    var thirdCategoryArray:[HomeCategoryModel] = []
    var fourthCategoryArray:[HomeCategoryModel] = []
    var counter = 0
    var newsList  = [NewsModelHome]()
    var bannerList = [BannerModelHome]()
    var aboutUsArray = [AboutUsModelHome]()
    var advArray = [AboutUsModelHome]()
    var endPoint = CGPoint()
    var scrollingPoint = CGPoint()
    var scrollingTimer : Timer!
    var carousalTimer: Timer?
    var newOffsetX: CGFloat = 0.0
    var timer = Timer()
    @IBOutlet weak var scrollViewLeadingConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newsCollectionView.layer.cornerRadius = newsCollectionView.frame.height / 2
        topCategoryCollectionView.register(UINib(nibName: "CategoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CategoryCell")
        secondCategoryCollectionView.register(UINib(nibName: "CategoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CategoryCell")
        thirdCategoryCollectionView.register(UINib(nibName: "CategoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CategoryCell")
        fourthCategoryCollectionView.register(UINib(nibName: "CategoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CategoryCell")
        slideCollectionView.register(UINib(nibName: "SlideCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SlideCell")
        
        menuTableView.tableFooterView = UIView()
        menuTableView.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuCell")
        homeTopArray = [
            HomeCategoryModel(imageName: "taxi_small", name: "വാഹനം"),
            HomeCategoryModel(imageName: "bus_icon", name: "ബസ് ടൈം"),
            HomeCategoryModel(imageName: "10", name: "ട്രയിൻ ടൈം"),
            HomeCategoryModel(imageName: "13", name: "അറിയിപ്പ്")
        ]
        
        menuArray = [
            HomeCategoryModel(imageName: "call_gray", name: "എമർജൻസി"),
//            HomeCategoryModel(imageName: "17", name: "ചരിത്രം"),
            HomeCategoryModel(imageName: "lightbulb", name: "ഞങ്ങളേക്കുറിച്ച്"),
            HomeCategoryModel(imageName: "user_icon", name: "അഡ്‌മിൻ"),
            HomeCategoryModel(imageName: "icon_ionic_ios_share", name: "ഷയര്‍ ചെയ്യുക"),
            HomeCategoryModel(imageName: "user_icon", name: "ഓട്ടോ വിവരം"),

        ]
        secondCategoryArray = [
            HomeCategoryModel(imageName: "blood-doner", name: "ബ്ലഡ് ബാങ്ക്"),
            HomeCategoryModel(imageName: "emergency", name: "എമർജൻസി"),
            HomeCategoryModel(imageName: "personal", name: "ഡയറക്ടറി"),
        ]
        thirdCategoryArray = [
            HomeCategoryModel(imageName: "medical", name: "ആരോഗ്യം"),
            HomeCategoryModel(imageName: "education", name: "വിദ്യാഭ്യാസം"),
            HomeCategoryModel(imageName: "library", name: "വായനശാല"),
        ]
        
        fourthCategoryArray = [
            HomeCategoryModel(imageName: "worker", name: "തൊഴിലാളികൾ"),
            HomeCategoryModel(imageName: "institution", name: "സ്ഥാപനങ്ങൾ"),
            HomeCategoryModel(imageName: "shop", name: "കടകൾ"),
            HomeCategoryModel(imageName: "eservice", name: "ഇ-സർവീസ്"),
            HomeCategoryModel(imageName: "application", name: "അപേക്ഷകൾ"),
            HomeCategoryModel(imageName: "ad", name: "പരസ്യങ്ങൾ")
        ]
        liveImageView.loadGif(name: "live")
        advImageView.loadGif(name: "ad1")
//
        menuScrollView.isHidden = true
        self.getNews()
        self.getHomeBanner()
        self.getAdvBoxDetail()
    }
    
    
    func startNewsTimer() {
        
        carousalTimer = Timer(fire: Date(), interval: 0.20, repeats: true) { (timer) in
            
            let initailPoint = CGPoint(x: self.newOffsetX,y :0)
            
            if __CGPointEqualToPoint(initailPoint, self.newsCollectionView.contentOffset) {
                
                if self.newOffsetX < self.newsCollectionView.contentSize.width {
                    self.newOffsetX += 2
                }
                if self.newOffsetX == self.newsCollectionView.contentSize.width {
                    self.newOffsetX = 0
                }
                
                self.newsCollectionView.contentOffset = CGPoint(x: self.newOffsetX,y :0)
                
            } else {
                self.newOffsetX = self.newsCollectionView.contentOffset.x
            }
        }
        
        RunLoop.current.add(carousalTimer!, forMode: .common)
    }
    
    @objc func nextPage() {
        // 1.back to the middle of sections
        let currentIndexPathReset = self.resetIndexPath()
        // 2.next position
        var nextItem: Int = currentIndexPathReset.item + 1
        if nextItem == self.slideArray.count {
            nextItem = 0
        }
        let nextIndexPath = IndexPath(item: nextItem, section: 0)
        // 3.scroll to next position
        self.slideCollectionView?.scrollToItem(at: nextIndexPath, at: .left, animated: true)
    }
    var x = 1

    @objc func scrollToNextCell(){
        
        //           get Collection View Instance
        let collectionView:UICollectionView;
        
        if self.x < self.slideArray.count {
            //           get cell size
            let cellSize = CGSize(width: self.view.frame.width, height: view.frame.height)
            //  CGSizeMake(self.view.frame.width, self.view.frame.height);
            
            //get current content Offset of the Collection view
            let contentOffset = slideCollectionView.contentOffset;
            
            //scroll to next cell
            slideCollectionView.scrollRectToVisible(CGRect(x: contentOffset.x + cellSize.width, y:  contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true);
            self.x = self.x + 1
        }else{
            self.x = 0
            self.slideCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        }
        
        
    }
    
    func moveCollectionToFrame(contentOffset : CGFloat) {

            let frame: CGRect = CGRect(x : contentOffset ,y : self.slideCollectionView.contentOffset.y ,width : self.slideCollectionView.frame.width,height : self.slideCollectionView.frame.height)
            self.slideCollectionView.scrollRectToVisible(frame, animated: true)
        }

    
    func startTimer() {
//        let kAutoScrollDuration: CGFloat = 4.0
//    Timer.scheduledTimer(timeInterval: TimeInterval(1), target: self, selector: #selector(self.scrollToNextCell), userInfo: nil, repeats: true)
        timer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true)
       
    }
    func resetIndexPath() -> IndexPath {
        // currentIndexPath
        let currentIndexPath: IndexPath? = self.slideCollectionView?.indexPathsForVisibleItems.last
        // back to the middle of sections
        let currentIndexPathReset = IndexPath(item: currentIndexPath!.item, section: 0)
        self.slideCollectionView?.scrollToItem(at: currentIndexPathReset, at: .left, animated: false)
        return currentIndexPathReset
    }
    @IBAction func onCloseMenu(_ sender: UIButton) {
        closeMenuWithAnimation()
    }
    
    @IBAction func onLive(_ sender: UIButton) {
        onNavigation(categoryId: 18, titleLabel: "ലൈവ് ന്യൂസ്", image: "18")
    }
    @IBAction func onMenu(_ sender: Any) {
        menuScrollView.isHidden = false
        self.scrollViewLeadingConstraint.constant = -self.view.bounds.size.width
        self.view.layoutIfNeeded()
        transparentView.alpha = 0
        self.view.endEditing(true)
        UIView.animate(withDuration: Double(0.5), animations: {
            self.scrollViewLeadingConstraint.constant = 0
            self.transparentView.alpha = 0.2
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func onAdvMain(_ sender: UIButton) {
        if let url =  advArray.first?.service_url{
            guard let url = URL(string: url) else { return }
            UIApplication.shared.open(url)
        }
    }
    
    func closeMenuWithAnimation() {
        UIView.animate(withDuration: Double(0.5), animations: {
            self.scrollViewLeadingConstraint.constant = -self.view.bounds.size.width
            self.transparentView.alpha = 0
            self.view.layoutIfNeeded()
        }, completion: { (finished: Bool) in
            self.menuScrollView.isHidden = true
        })
    }
}
extension HomeScreen : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == topCategoryCollectionView{
            return homeTopArray.count
        }else if collectionView == slideCollectionView{
            return self.slideArray.isEmpty ? 0 : slideArray.count
        }else if collectionView == secondCategoryCollectionView {
            return secondCategoryArray.count
        }else if collectionView == thirdCategoryCollectionView{
            return thirdCategoryArray.count
        }
        else if collectionView == newsCollectionView {
            return newsList.count
        }else{
            return fourthCategoryArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == topCategoryCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCollectionViewCell
            let model = homeTopArray[indexPath.row]
            cell.nameLabel.text = model.name
            cell.categoryImageView.image = UIImage(named: model.imageName!)
            return cell
        }else if collectionView == slideCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SlideCell", for: indexPath) as! SlideCollectionViewCell
            if slideArray.count>0{
                let model = slideArray[indexPath.row]
                Utility.setImage("\(ICON_URL)\(model.image_path ?? "")", imageView: cell.slideImageView)
            }
            return cell
        }else if collectionView == secondCategoryCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCollectionViewCell
            let model = secondCategoryArray[indexPath.row]
            cell.nameLabel.text = model.name
            cell.categoryBackgroundView.backgroundColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9254901961, alpha: 1)
            cell.categoryBackgroundView.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            cell.categoryBackgroundView.layer.borderWidth = 0.5
            cell.categoryImageView.image = UIImage(named: model.imageName!)
            return cell
        }else if collectionView == thirdCategoryCollectionView{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCollectionViewCell
            let model = thirdCategoryArray[indexPath.row]
            cell.nameLabel.text = model.name
            cell.categoryBackgroundView.backgroundColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9254901961, alpha: 1)
            cell.categoryBackgroundView.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            cell.categoryBackgroundView.layer.borderWidth = 0.5
            cell.categoryImageView.image = UIImage(named: model.imageName!)
            return cell
            
        }
        else if collectionView == newsCollectionView {
            let cell = newsCollectionView.dequeueReusableCell(withReuseIdentifier: "newsCell", for: indexPath)
            let newsHeadingLabel = cell.viewWithTag(101)as! UILabel
            let newsObj = newsList[indexPath.item]
            if newsObj.scroll_type == "1" {
                newsHeadingLabel.backgroundColor = #colorLiteral(red: 0.431372549, green: 0.8588235294, blue: 0.462745098, alpha: 1)
            }
            else{
                newsHeadingLabel.backgroundColor = #colorLiteral(red: 0.2734926045, green: 0.6934072971, blue: 0.877043426, alpha: 1)
            }
            newsHeadingLabel.text = newsObj.Newsheading
            newsHeadingLabel.layer.cornerRadius = 5
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCollectionViewCell
            let model = fourthCategoryArray[indexPath.row]
            cell.nameLabel.text = model.name
            cell.categoryBackgroundView.backgroundColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9254901961, alpha: 1)
            cell.categoryBackgroundView.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            cell.categoryBackgroundView.layer.borderWidth = 0.5
            cell.categoryImageView.image = UIImage(named: model.imageName!)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == topCategoryCollectionView{
            let model = homeTopArray[indexPath.row]
            if indexPath.row == 0{
                onNavigation(categoryId: 0, titleLabel: "വാഹനം", image: model.imageName!)
            }else if indexPath.row == 1{
                onNavigation(categoryId: 9, titleLabel: "ബസ് ടൈം", image: model.imageName!)
            }else if indexPath.row == 2{
                onNavigation(categoryId: 10, titleLabel: "ട്രയിൻ ടൈം", image: model.imageName!)
            }else{
                let storyBoard = UIStoryboard(name: "Home", bundle: nil)
                let control = storyBoard.instantiateViewController(withIdentifier: "SoundScreen") as! SoundScreen
                control.categoryId = 13
                control.titleText = "അറിയിപ്പ്"
                self.navigationController?.pushViewController(control, animated: true)
            }
        }else  if collectionView == secondCategoryCollectionView{
            
            let model = secondCategoryArray[indexPath.row]
            if indexPath.row == 0{
                onNavigation(categoryId: 12, titleLabel: "ബ്ലഡ് ബാങ്ക്", image: model.imageName!)
            }else if indexPath.row == 1{
                onNavigation(categoryId: 1, titleLabel: "എമർജൻസി", image: model.imageName!)
            }else{
                onNavigation(categoryId: 11, titleLabel: "വ്യക്തിഗതം", image: "11")
            }
        }else  if collectionView == thirdCategoryCollectionView{
            
            let model = thirdCategoryArray[indexPath.row]
            if indexPath.row == 0{
                onNavigation(categoryId: 2, titleLabel: "ആരോഗ്യം", image: model.imageName!)
            }else if indexPath.row == 1{
                onNavigation(categoryId: 3, titleLabel: "വിദ്യാഭ്യാസം", image: model.imageName!)
            }else{
                onNavigation(categoryId: 14, titleLabel: "വായനശാല", image: "14")
            }
        }else  if collectionView == fourthCategoryCollectionView{
            let model =  fourthCategoryArray[indexPath.row]
            if indexPath.row == 0{
                onNavigation(categoryId: 4, titleLabel: "തൊഴിലാളികൾ", image: model.imageName!)
            }else if indexPath.row == 1{
                onNavigation(categoryId: 5, titleLabel: "സ്ഥാപനങ്ങൾ", image: model.imageName!)
            } else if indexPath.row == 2{
                onNavigation(categoryId: 6, titleLabel: "കടകൾ", image: model.imageName!)
            }else if indexPath.row == 3{
                onNavigation(categoryId: 16, titleLabel: "ഇ-സർവീസ്", image:  "16")
            }else if indexPath.row == 4{
                onNavigation(categoryId: 15, titleLabel: "അപേക്ഷകൾ", image:  "15")
            }else{
                onNavigation(categoryId: 7, titleLabel: "പരസ്യങ്ങൾ", image:  "7")
            }
        }
        else if collectionView == newsCollectionView {
            
            let newsObj = newsList[indexPath.item]
            if newsObj.scroll_type == "1" {
                let storyBoard = UIStoryboard(name: "Home", bundle: nil)
                let control = storyBoard.instantiateViewController(withIdentifier: "NewsDetailsScreen") as! NewsDetailsScreen
                control.newsCategoryId = newsObj.sub_cat_id
                self.navigationController?.pushViewController(control, animated: true)
            }
            else{
                if newsObj.service_url != "" {
                    guard let url = URL(string: newsObj.service_url) else { return }
                    UIApplication.shared.open(url)
                }
            }
        }
        if collectionView == slideCollectionView{
            let model = slideArray[indexPath.row]
            //                 if indexPath.row == 0{
            //
            //
            //                     onNavigation(categoryId: 0, titleLabel: "വാഹനം", image: "")
            //                 }else if indexPath.row == 1{
            //                     onNavigation(categoryId: 9, titleLabel: "ബസ് ടൈം", image: "")
            //                 }else if indexPath.row == 2{
            //                     onNavigation(categoryId: 10, titleLabel: "ട്രയിൻ ടൈം", image: "")
            //                 }else{
            //                     let storyBoard = UIStoryboard(name: "Home", bundle: nil)
            //                     let control = storyBoard.instantiateViewController(withIdentifier: "SoundScreen") as! SoundScreen
            //                     control.categoryId = 13
            //                     control.titleText = "അറിയിപ്പ്"
            //                     self.navigationController?.pushViewController(control, animated: true)
            //                 }
            
 
            
            guard let url = URL(string:  model.service_url) else { return }
            UIApplication.shared.open(url)
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == topCategoryCollectionView{
            return CGSize(width: (UIScreen.main.bounds.width/4)-8, height: collectionView.frame.height-10)
        }else if collectionView == slideCollectionView{
            return CGSize(width: (UIScreen.main.bounds.width), height: collectionView.frame.height)
        }else if collectionView == secondCategoryCollectionView{
            return CGSize(width: (collectionView.frame.width/3)-4, height: collectionView.frame.height)
        }else if collectionView == thirdCategoryCollectionView{
            return CGSize(width: (collectionView.frame.width/3)-4, height: collectionView.frame.height)
        }
        else if collectionView == newsCollectionView {
            let label = UILabel(frame: CGRect.zero)
            label.text = newsList[indexPath.item].Newsheading
            label.sizeToFit()
            return CGSize(width: (label.frame.width), height: 34)
        }else{
            return CGSize(width: (collectionView.frame.width/3)-4, height: collectionView.frame.height/2-8)
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == slideCollectionView{
            DispatchQueue.main.async {
                self.pageControllerDot.currentPage = indexPath.item
            }
            
        }
    }
    
    //MARK:- API METHOD
    func getNews() {
        let url = "https://pallisseri.com/eps/ios-api/ios/getScrollNewsHome.php?main_cat_id=22"
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getNewsHomePage(url: url, complition: { [self] (newsData) in
            Utility.hideIndicator()
            print("News ->",newsData)
            self.newsList = newsData.result!
            if self.newsList.count > 0 {
                self.newsCollectionView.reloadData()
                self.startNewsTimer()
            }
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
    
    func getHomeBanner() {
        let url = "https://pallisseri.com/eps/ios-api/ios/getBanner.php?main_cat_id=99"
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getBannerHomePage(url: url, complition: { [self] (bannerData) in
            Utility.hideIndicator()
            print("BAnner ->",bannerData)
            self.slideArray = bannerData.result!
            if self.slideArray.count > 0{
                self.pageControllerDot.numberOfPages = self.slideArray.count
                self.pageControllerDot.currentPage = 0
                self.slideCollectionView.reloadData()
                startTimer()
            }
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
    
    //https://pallisseri.com/eps/ios-api/ios/getAdboxMinor.php?sub_cat_id=154&page_id=3
    
    func PersonDetail() {
        let url = "https://pallisseri.com/eps/ios-api/ios/ListSubCategory.php?main_cat_id=19"
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getAboutUsPage(url: url, complition: { (newsData) in
            Utility.hideIndicator()
            print("About Us ->",newsData)
            self.aboutUsArray = newsData.result!
            //self.dataTableView.reloadData()
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
    func getAdvBoxDetail() {
        let url = "https://pallisseri.com/eps/ios-api/ios/getAdboxMainPage.php"
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getAboutUsPage(url: url, complition: { (newsData) in
            Utility.hideIndicator()
            print("About Us ->",newsData)
//            self.aboutUsArray = newsData.result!
            if newsData.result?.count ?? 0 > 0{
                self.advArray = newsData.result!
                let image = self.advArray[0].image_path
                let str = "\(ICON_URL)\(image ?? "")"
                Utility.setImage(str, imageView: self.advImageView)
            }
           
            //self.dataTableView.reloadData()
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
    func ShareDetail() {
        let url = "https://pallisseri.com/eps/ios-api/ios/ListSubCategory.php?main_cat_id=21"
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getAboutUsPage(url: url, complition: { (newsData) in
            Utility.hideIndicator()
            print("About Us ->",newsData)
            self.aboutUsArray = newsData.result!
            //self.dataTableView.reloadData()
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
    
    func onNavigation(categoryId:Int,titleLabel:String,image:String){
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "InnerCategoryScreen") as! InnerCategoryScreen
        control.categoryId = categoryId
        control.titleLabel = titleLabel
        control.imageString = image
        self.navigationController?.pushViewController(control, animated: true)
    }
}

extension HomeScreen : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuTableViewCell
        let model = menuArray[indexPath.row]
        cell.menuImageView.image = UIImage(named: model.imageName!)
        cell.menuLabel.text = model.name
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = menuArray[indexPath.row]
        if indexPath.row == 0{
            onNavigation(categoryId: 1, titleLabel: "എമർജൻസി", image: model.imageName!)
        }else if indexPath.row == 1{
//            onNavigation(categoryId: 20, titleLabel: "About Us")
            let storyBoard = UIStoryboard(name: "Home", bundle: nil)
            let control = storyBoard.instantiateViewController(withIdentifier: "ShareScreen") as! ShareScreen
            control.type = 1
            self.navigationController?.pushViewController(control, animated: true)
        }else if indexPath.row == 2{
//            onNavigation(categoryId: 19, titleLabel: "അഡ്‌മിൻ ")
            let storyBoard = UIStoryboard(name: "Home", bundle: nil)
            let control = storyBoard.instantiateViewController(withIdentifier: "UserScreen") as! UserScreen
            self.navigationController?.pushViewController(control, animated: true)
        }else if indexPath.row == 3{
//            onNavigation(categoryId: 21, titleLabel: "ആപ്പ് ഷെയർ ചെയ്യൂ")
            let storyBoard = UIStoryboard(name: "Home", bundle: nil)
            let control = storyBoard.instantiateViewController(withIdentifier: "ShareScreen") as! ShareScreen
            control.type = 0
            self.navigationController?.pushViewController(control, animated: true)
        }else if indexPath.row == 4{
            let storyBoard = UIStoryboard(name: "Home", bundle: nil)
            let control = storyBoard.instantiateViewController(withIdentifier: "StatusUpdateScreen") as! StatusUpdateScreen
            self.navigationController?.pushViewController(control, animated: true)
        }
    }
    
}
