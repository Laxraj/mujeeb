//
//  MinorInnerScreen.swift
//  Mujeeb
//
//  Created by iroid on 01/12/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class MinorInnerScreen: UIViewController {

    @IBOutlet weak var catogoryTitleLabel: UILabel!
    @IBOutlet weak var innerCategoryCollectionView: UICollectionView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imgViewHeight: NSLayoutConstraint!
    var categoryArray:[CategoryData] = []
    var diffcategoryArray:[InnerData] = []
    var searchCategoryArray: [InnerData] = []
    
        var categoryId = "0"
        var titleLabel = ""
        var bloodGroup = ""
        var isSearchShow = true
        var start: Int = 1
    var serviceUrl = ""
    var mainCategoryId = 0
        override func viewDidLoad() {
            super.viewDidLoad()
            self.initialDetails()
            innerCategoryCollectionView.register(UINib(nibName: "InnerCategoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "innerCategoryCell")
            innerCategoryCollectionView.register(UINib(nibName: "WardCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "WardCollectionViewCell")
            innerCategoryCollectionView.register(UINib(nibName: "BloodListCollectionView", bundle: nil), forCellWithReuseIdentifier: "BloodListCollectionView")
            catogoryTitleLabel.text = titleLabel
            self.searchTextField.addTarget(self, action: #selector(self.textFieldChange(textField:)), for: .editingChanged)
            
            if !isSearchShow {
                searchViewHeight.constant = 0
                searchView.isHidden = true
                
            }
            //self.im
        }
    
    
        @objc func textFieldChange(textField: UITextField){
            if textField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
                self.searchCategoryArray = []
                self.innerCategoryCollectionView.reloadData()
            }
        }
        
        // MARK: - Button Click
        
        @IBAction func onBack(_ sender: Any) {
            self.navigationController?.popViewController(animated: true)
        }
        
        @IBAction func onHome(_ sender: Any) {
            let storyBoard = UIStoryboard(name: "Home", bundle: nil)
            let control = storyBoard.instantiateViewController(withIdentifier: "HomeScreen") as! HomeScreen
            self.navigationController?.pushViewController(control, animated: false)
        }
        
        // MARK: - Initial Details
        func initialDetails() {
            //
            if categoryId == "0"{
                getCategory()
            }else if categoryId == "100"{
                self.getPersonDetails(id: self.categoryId)
            }else if categoryId == "12"{
                self.getBloodDetails(bloodGroup: self.bloodGroup)
            }else{
                anotherCategory(id: categoryId)
            }
//            if categoryId == "152"{
                self.showImage()
//            }
        }
    
    @IBAction func onSearch(_ sender: Any) {
        if self.searchTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).count > 0{
            if self.categoryId == "12"{
                self.searchBloodDetails(id: self.categoryId, text: self.searchTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines))
            }else{
                self.searchPersonDetails(id: self.categoryId, text: self.searchTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines))
            }
        }
    }
    
    @IBAction func onAdv(_ sender: UIButton) {
        guard let url = URL(string: serviceUrl) else { return }
        UIApplication.shared.open(url)
    }
    @objc func onCallClick(sender: UIButton){
        let item = self.searchCategoryArray.count > 0 ? self.searchCategoryArray[sender.tag] : diffcategoryArray[sender.tag]
        if let phoneNumber = item.contact_number1{
            Utility.openDialPad(phone: phoneNumber)
        }
    }
    
    @objc func onWhatsappClick(sender: UIButton){
        let item = self.searchCategoryArray.count > 0 ? self.searchCategoryArray[sender.tag] : diffcategoryArray[sender.tag]
        if let phoneNumber = item.contact_number1{
            Utility.openChatWhatsapp(phone: phoneNumber)
        }
    }
    
    @objc func onShareClick(sender: UIButton){
//        if let phoneNumber = self.innerCategoryArray[sender.tag].contact_number1{
            Utility.shareMessage(viewController: self, text: "Test Message")
//        }
    }
    
    //MARK:- SET IMAGE
    func showImage() {
        let url = "https://pallisseri.com/eps/ios-api/ios/getAdboxMinor.php?sub_cat_id=\(categoryId)&page_id=3"
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getInnerCategory(url: url, complition: { (newsData) in
            Utility.hideIndicator()
            print("ContactNumber",newsData)
//            self.navigationController?.popViewController(animated: true)
            //self.dataTableView.reloadData()
           
            if let imagePath =  newsData.result?.first?.image_path{
                let str = "\(ICON_URL)\(imagePath)"
                self.imgView.image = UIImage.gif(url: str)
                self.imgViewHeight.constant = 100
                self.serviceUrl =  newsData.result?.first?.service_url ?? ""
            }else{
                self.imgViewHeight.constant = 0
            }
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
}

    extension MinorInnerScreen : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            if categoryId == "0"{
                return categoryArray.count
            }else if self.searchTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).count > 0{
                return self.searchCategoryArray.count
            } else{
               return diffcategoryArray.count
            }
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            if categoryId == "100"{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WardCollectionViewCell", for: indexPath) as! WardCollectionViewCell
                let model = self.searchCategoryArray.count > 0 ? self.searchCategoryArray[indexPath.row] : diffcategoryArray[indexPath.row]
                cell.userNameLAbel.text = model.name
                cell.mobileLabel.text = model.contact_number1
                cell.addessLabel.text = model.Address
                cell.wardLabel.text = "WARD "+(model.ward_no ?? "")
                cell.onCall.tag = indexPath.row
                cell.onWhatsapp.tag = indexPath.row
                cell.onWhatsapp.addTarget(self, action: #selector(self.onWhatsappClick(sender:)), for: .touchUpInside)
                cell.onCall.addTarget(self, action: #selector(self.onCallClick(sender:)), for: .touchUpInside)
                cell.onShare.tag = indexPath.row
                cell.onShare.addTarget(self, action: #selector(self.onShareClick(sender:)), for: .touchUpInside)
                return cell
            }else if categoryId == "12"{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BloodListCollectionView", for: indexPath) as! BloodListCollectionView
                let model = self.searchCategoryArray.count > 0 ? self.searchCategoryArray[indexPath.row] : diffcategoryArray[indexPath.row]
                cell.userNameLabel.text = model.name
                cell.mobileLabel.text = model.contact_number1
                cell.addressLabel.text = model.Address
                cell.groupLabel.text = model.BloodGroup ?? ""
                cell.onWhatsapp.tag = indexPath.row
                cell.onCall.tag = indexPath.row
                cell.onWhatsapp.addTarget(self, action: #selector(self.onWhatsappClick(sender:)), for: .touchUpInside)
                cell.onCall.addTarget(self, action: #selector(self.onCallClick(sender:)), for: .touchUpInside)
                cell.onShare.tag = indexPath.row
                cell.onShare.addTarget(self, action: #selector(self.onShareClick(sender:)), for: .touchUpInside)
                return cell
            }
           
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "innerCategoryCell", for: indexPath) as! InnerCategoryCollectionViewCell
                if categoryId == "0"{
                    let model = categoryArray[indexPath.row]
                    cell.categoryLabel.text = model.taxi_category
                    let str = "\(ICON_URL)\(model.icon_path ?? "")"
                    Utility.setImage(str, imageView: cell.categoryImageView)
                    
                }else{
                    let model = diffcategoryArray[indexPath.row]
                    cell.categoryLabel.text = model.minor_cat_name
                    let str = "\(ICON_URL)\(model.icon_path ?? "")"
                    Utility.setImage(str, imageView: cell.categoryImageView)
                }
                 return cell
           
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
            if categoryId == "0"{
                let model = categoryArray[indexPath.row]
                let storyBoard = UIStoryboard(name: "Home", bundle: nil)
                let control = storyBoard.instantiateViewController(withIdentifier: "InnerDataScreen") as! InnerDataScreen
                control.id = model.id!
                control.title = model.taxi_category
                self.navigationController?.pushViewController(control, animated: true)
            }
            else{
                let model = diffcategoryArray[indexPath.row]
                let str = "\(ICON_URL)\(model.icon_path ?? "")"
                let storyBoard = UIStoryboard(name: "Contact", bundle: nil)
                let control = storyBoard.instantiateViewController(withIdentifier: "ContactDetailScreen") as! ContactDetailScreen
                control.id = model.sub_cat_id ?? ""
                control.hasMinor = model.minor_cat_id ?? ""
                control.title = model.minor_cat_name
                 control.imagePath = str
                control.mainCategoryId = mainCategoryId
                self.navigationController?.pushViewController(control, animated: true)
            }
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            if categoryId == "100" || categoryId == "12"{
                return CGSize(width: (UIScreen.main.bounds.width), height: 170)
            }else if categoryId == "0"{
                if indexPath.row == categoryArray.count - 1 {
                   if categoryArray.count  % 2 != 0 {
                        let width = (UIScreen.main.bounds.width)
                        return CGSize(width: width, height: 90)
                    }else{
                        let width = (UIScreen.main.bounds.width / 2)
                        return CGSize(width: width, height: 90)
                    }
                }else{
                    let width = (UIScreen.main.bounds.width / 2)
                    return CGSize(width: width, height: 90)
                }
                
                
            }else{
                if indexPath.row == diffcategoryArray.count - 1 {
                    if diffcategoryArray.count  % 2 != 0 {
                    let width = (UIScreen.main.bounds.width)
                    return CGSize(width: width, height: 90)
                }else{
                    let width = (UIScreen.main.bounds.width / 2)
                    return CGSize(width: width, height: 120)
                }
                 }else{
                    let width = (UIScreen.main.bounds.width / 2)
                    return CGSize(width: width, height: 120)
                }
                
    //            if (indexPath.row == diffcategoryArray.count - 1 ) { //it's your last cell
    //                  //Load more data & reload your collection view
    //                let isEven = indexPath.row % 2 == 0
    //                if isEditing{
    //
    //                }else{
    //
    //                }
    //
    //                }
            }
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 0
        }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 0
        }
        
        func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
            if self.categoryId == "100"{
                if self.searchCategoryArray.isEmpty{
                    if self.diffcategoryArray.count - 3 == indexPath.row{
                        self.start += 15
                        self.getPersonDetails(id: categoryId)
                    }
                }
            }
        }
        
        //MARK:- API METHOD
        func getCategory() {
            let url = BASE_URL +  API.GET_TAXI_CATEGORY
            print(url)
            Utility.showIndecator()
            CategoryApiManager.shared.getCategory(url: url, complition: { (categoryData) in
                Utility.hideIndicator()
                print(categoryData)
                self.categoryArray = categoryData
                self.innerCategoryCollectionView.reloadData()
            }) { (msg) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: msg)
            }
        }
        
        func anotherCategory(id:String) {
            print(id)
            let url = BASE_URL +  API.INNER_SUB_CATEGORY + "\(id)"
            print(url)
            Utility.showIndecator()
            CategoryApiManager.shared.getInnerCategory(url: url, complition: { (categoryData) in
                Utility.hideIndicator()
                print(categoryData)
                self.diffcategoryArray = categoryData.result!
                self.innerCategoryCollectionView.reloadData()
            }) { (msg) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: msg)
            }
        }
        
        //MARK:- Personal Details API
        func getPersonDetails(id:String) {
            print(id)
            let url = BASE_URL +  API.GET_PERSON_DETAILS + "11" + "&sub_cat_id=\(id)&start=\(self.start)"+"&limit=15"
            print(url)
            Utility.showIndecator()
            CategoryApiManager.shared.getInnerCategory(url: url, complition: { (categoryData) in
                Utility.hideIndicator()
                print(categoryData)
                if self.start > 1{
                    self.appendDataCollectionView(data: categoryData.result!)
                }else{
                    self.diffcategoryArray = categoryData.result!
                    self.innerCategoryCollectionView.reloadData()
                }
            }) { (msg) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: msg)
            }
        }
        
        func searchPersonDetails(id:String,text: String) {
            print(id)
            let url = BASE_URL +  API.GET_SEARCH_PERSON_DETAILS + "11" + "&sub_cat_id=\(id)&searchtag="+text
            print(url)
            Utility.showIndecator()
            CategoryApiManager.shared.getInnerCategory(url: url, complition: { (categoryData) in
                Utility.hideIndicator()
                print(categoryData)
                self.searchCategoryArray = categoryData.result!
                self.innerCategoryCollectionView.reloadData()
            }) { (msg) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: msg)
            }
        }
        
        //MARK:- Blood Deatils API
        func getBloodDetails(bloodGroup:String) {
//            print(id)
            let url = BASE_URL +  API.GET_BLOOD_DETAILS + "11" + "&BloodGroup="+bloodGroup
            print(url)
            Utility.showIndecator()
            CategoryApiManager.shared.getInnerCategory(url: url, complition: { (categoryData) in
                Utility.hideIndicator()
                print(categoryData)
                if self.start > 1{
                    self.appendDataCollectionView(data: categoryData.result!)
                }else{
                    self.diffcategoryArray = categoryData.result!
                    self.innerCategoryCollectionView.reloadData()
                }
            }) { (msg) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: msg)
            }
        }
        
        func searchBloodDetails(id:String,text: String) {
            print(id)
            let url = BASE_URL +  API.SEARCH_BLOOD_DETAILS + "11" + "&group=\(self.bloodGroup)&searchtag="+text
            print(url)
            Utility.showIndecator()
            CategoryApiManager.shared.getInnerCategory(url: url, complition: { (categoryData) in
                Utility.hideIndicator()
                print(categoryData)
                self.searchCategoryArray = categoryData.result!
                self.innerCategoryCollectionView.reloadData()
            }) { (msg) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: msg)
            }
        }
        
        //MARK:- cELL SCROLL PAGINGATION
//        func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//            var visibleRect = CGRect()
//
//            visibleRect.origin = self.innerCategoryCollectionView.contentOffset
//            visibleRect.size = self.innerCategoryCollectionView.bounds.size
//
//            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
//
//            guard let indexPath = self.innerCategoryCollectionView.indexPathForItem(at: visiblePoint) else { return }
//
////            print(indexPath)
////
//        }
        
        func appendDataCollectionView(data: [InnerData]){
            let data = data
            var indexPathArray: [IndexPath] = []
            for i in self.diffcategoryArray.count..<self.diffcategoryArray.count + data.count{
                indexPathArray.append(IndexPath(item: i, section: 0))
            }
            self.diffcategoryArray.append(contentsOf: data)
            self.innerCategoryCollectionView.insertItems(at: indexPathArray)
        }
    }
