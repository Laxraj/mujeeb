//
//  ShareScreen.swift
//  Mujeeb
//
//  Created by Kishan on 08/11/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class ShareScreen: UIViewController {

    @IBOutlet weak var aboutUsView: dateSportView!
    @IBOutlet weak var aboutUsText: UILabel!
      
    var type = Int() //Share = 0 and AboutUs = 1
    var aboutUsArray = [AboutUsModelHome]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if type == 1 {
            self.aboutUs()
            aboutUsView.isHidden = false
        }
        else{
            self.share()
            aboutUsView.isHidden = true
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onHome(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "HomeScreen") as! HomeScreen
        self.navigationController?.pushViewController(control, animated: false)
    }
    
    @IBAction func onShare(_ sender: Any) {
    }
    
    //MARK: API
    func aboutUs() {
        let url = "https://pallisseri.com/eps/ios-api/ios/getAboutusDetails.php?main_cat_id=20"
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getAboutUsPage(url: url, complition: { (newsData) in
            Utility.hideIndicator()
            print("About Us ->",newsData)
            self.aboutUsArray = newsData.result!
            self.aboutUsText.text = self.aboutUsArray[0].details
            //self.dataTableView.reloadData()
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
    
    func share() {
        let url = "https://pallisseri.com/eps/ios-api/ios/getAppDetails.php"
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getAboutUsPage(url: url, complition: { (newsData) in
            Utility.hideIndicator()
            print("Share ->",newsData)
            self.aboutUsArray = newsData.result!
            //self.dataTableView.reloadData()
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
}
