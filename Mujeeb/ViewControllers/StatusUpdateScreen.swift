//
//  StatusUpdateScreen.swift
//  Mujeeb
//
//  Created by iroid on 31/12/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class StatusUpdateScreen: UIViewController {

    @IBOutlet weak var dataTitleLabel: UILabel!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
   
    func submit() {
        let url = "https://pallisseri.com/eps/ios-api/ios/getTaxiphone.php?ph_number=\(mobileNumberTextField.text ?? "")"
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getBannerHomePage(url: url, complition: { [self] (bannerData) in
            Utility.hideIndicator()
            print("BAnner ->",bannerData)
//            self.slideArray = bannerData.result!
//            if self.slideArray.count > 0{
//                self.pageControllerDot.numberOfPages = self.bannerList.count
//                self.pageControllerDot.currentPage = 0
//                self.slideCollectionView.reloadData()
//                self.startTimer()
            self.navigationController?.popViewController(animated: true)
//            }
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
        
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        submit()
        
    }
    @IBAction func onHome(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "HomeScreen") as! HomeScreen
        self.navigationController?.pushViewController(control, animated: false)
    }
    
    
    
  
}
