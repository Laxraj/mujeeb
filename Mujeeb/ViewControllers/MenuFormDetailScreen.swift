//
//  MenuFormDetailScreen.swift
//  Mujeeb
//
//  Created by iroid on 05/12/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class MenuFormDetailScreen: UIViewController {
    
    @IBOutlet weak var aboutUsView: dateSportView!
    @IBOutlet weak var aboutUsText: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    var titleStr = ""
    var aboutUsArray = [AboutUsModelHome]()
        var id = "0"
          var minorCatId = "0"
          var hasMinor = "0"
        override func viewDidLoad() {
            super.viewDidLoad()
                self.historyDetails()
            
            titleLabel.text = titleStr
        }
        
        @IBAction func onBack(_ sender: Any) {
            self.navigationController?.popViewController(animated: true)
        }
        
        @IBAction func onHome(_ sender: Any) {
            let storyBoard = UIStoryboard(name: "Home", bundle: nil)
            let control = storyBoard.instantiateViewController(withIdentifier: "HomeScreen") as! HomeScreen
            self.navigationController?.pushViewController(control, animated: false)
        }
        
        
        
        //MARK: API
        func historyDetails() {
            let url = "https://pallisseri.com/eps/ios-api/ios/getHistoryDetails.php?main_cat_id=\(id)&sub_cat_id=\(hasMinor)"
            print(url)
            Utility.showIndecator()
            CategoryApiManager.shared.getAboutUsPage(url: url, complition: { (newsData) in
                Utility.hideIndicator()
                print("About Us ->",newsData)
                self.aboutUsArray = newsData.result!
                self.aboutUsText.text = self.aboutUsArray[0].history
                //self.dataTableView.reloadData()
            }) { (msg) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: msg)
            }
        }
        
      
    }
