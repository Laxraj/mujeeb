//
//  NewsDetailsScreen.swift
//  Mujeeb
//
//  Created by Kishan on 21/11/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class NewsDetailsScreen: UIViewController {

    @IBOutlet weak var newListTableView: UITableView!
    
    var newsList = [newsDetailsModelHome]()
    var newsCategoryId = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.newListTableView.register(UINib(nibName: "NewsDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "newsDetailsCell")
        self.newsDetails()
        // Do any additional setup after loading the view.
    }
    

    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onHome(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "HomeScreen") as! HomeScreen
        self.navigationController?.pushViewController(control, animated: false)
    }

    //MARK: API
    func newsDetails() {
        let url = "https://pallisseri.com/eps/ios-api/ios/ScrollNewsPageContent.php?news_id=\(newsCategoryId)"
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getNewsDetailsPage(url: url, complition: { (newsData) in
            Utility.hideIndicator()
            print("About Us ->",newsData)
            self.newsList = newsData.result!
            if self.newsList.count > 0 {
                self.newListTableView.reloadData()
            }
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
}


extension NewsDetailsScreen : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = newListTableView.dequeueReusableCell(withIdentifier: "newsDetailsCell", for: indexPath)as! NewsDetailsTableViewCell
        let newsObj = newsList[indexPath.item]
        cell.newsHeadLineLabel.text = newsObj.para_heading
        cell.newsDiscriptionLabel.text = newsObj.detailedNews
        cell.selectionStyle = .none
        tableView.tableFooterView = .none
        return cell
    }

}
