//
//  BloodDonateScreen.swift
//  Mujeeb
//
//  Created by iroid on 06/11/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class BloodDonateScreen: UIViewController {

    
    @IBOutlet weak var titleLabel: UILabel!
    
    var categoryArray:[CategoryData] = []
    var categoryId = "0"
    var titletext = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onHome(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "HomeScreen") as! HomeScreen
        self.navigationController?.pushViewController(control, animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
