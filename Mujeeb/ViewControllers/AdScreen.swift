//
//  AdScreen.swift
//  Mujeeb
//
//  Created by iroid on 10/01/21.
//  Copyright © 2021 iroid. All rights reserved.
//

import UIKit

class AdScreen: UIViewController {
    
    @IBOutlet weak var advTableView: UITableView!
    
    @IBOutlet weak var titleLabel: UILabel!
    var id = "0"
    var minorCatId = "0"
    var hasMinor = "0"
    var innerCategoryArray:[InnerData] = []
    var titleString = String()
    var imagePath = ""
    var taxiDetailsId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = titleString
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getCategory()
    }
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onHome(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "HomeScreen") as! HomeScreen
        self.navigationController?.pushViewController(control, animated: false)
    }
    @IBAction func onAddForm(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard(name: "Contact", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "ContactFormScreen") as! ContactFormScreen
        control.subCatId = id
        control.minor_cat_id = hasMinor
        control.categoryName = titleString
        self.navigationController?.pushViewController(control, animated: false)
    }
    
    @IBAction func onCall(_ sender: UIButton) {
    }
    @IBAction func onWhatsapp(_ sender: UIButton) {
    }
    @IBAction func onShare(_ sender: UIButton) {
        
    }
    
    func getCategory() {
        let url = BASE_URL +  API.GET_CONTACT_DETAIL + id + "&minor_cat_id=" + hasMinor
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getInnerCategory(url: url, complition: { (categoryData) in
            Utility.hideIndicator()
            print(categoryData)
            self.innerCategoryArray = categoryData.result!
            if self.innerCategoryArray.count > 0{
                self.advTableView.isHidden = false
                // self.noDataFoundView.isHidden  = true
            }else{
                self.advTableView.isHidden = true
                // self.noDataFoundView.isHidden  = false
            }
            self.advTableView.reloadData()
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
    
}
extension AdScreen:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return innerCategoryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "adCell", for: indexPath)
        let postDataLabel = cell.viewWithTag(109) as! UILabel
        let iconImageView = cell.viewWithTag(101) as! UIImageView
        let titleLabel = cell.viewWithTag(102) as! UILabel
        let categoryLabel = cell.viewWithTag(103) as! UILabel
        let locationLabel = cell.viewWithTag(105) as! UILabel
        let detailLabel = cell.viewWithTag(107) as! UILabel
        let numLabelLabel = cell.viewWithTag(108) as! UILabel
        let model = innerCategoryArray[indexPath.row]
        titleLabel.text = model.institution_name
        categoryLabel.text = model.contact_person
        locationLabel.text = model.Address
        postDataLabel.text = "Posted : " + "\(model.dt_created ?? "")"
        //        locationLabel.text = model.driver_address
        numLabelLabel.text = model.contact_number1
        detailLabel.text = model.description
        print(model.description)
        if model.icon_path != nil && model.icon_path != ""  {
            let str = "\(ICON_URL)\(model.icon_path ?? "")"
            Utility.setImage(str, imageView:iconImageView)
        }else{
            Utility.setImage(imagePath, imageView: iconImageView)
        }
        return cell
    }
}
