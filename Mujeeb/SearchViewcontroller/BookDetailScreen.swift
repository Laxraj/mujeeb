//
//  BookDetailScreen.swift
//  Mujeeb
//
//  Created by iroid on 27/12/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class BookDetailScreen: UIViewController {

    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var bookDetailTableView: UITableView!
    var titleText = ""
    var booksArray:[InnerData] = []
    var searchCategoryArray: [InnerData] = []
    var innerData:InnerData?
    var start: Int = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bookDetailTableView.register(UINib(nibName: "BookTableViewCell", bundle: nil), forCellReuseIdentifier: "BookDetailCell")
        titleLabel.text = titleText
         self.searchTextField.addTarget(self, action: #selector(self.textFieldChange(textField:)), for: .editingChanged)
        initialDetails()
    }
    // MARK: - Initial Details
        func initialDetails() {
            //
           // innerData
            bookDetail(id: innerData?.main_cat_id ?? "", subCategoryId: innerData?.sub_cat_id ?? "", start: "\(start)", limite: "15")
           
        }
   @objc func textFieldChange(textField: UITextField){
              if textField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
                  self.searchCategoryArray = []
                  self.bookDetailTableView.reloadData()
              }
          }
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onHome(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "HomeScreen") as! HomeScreen
        self.navigationController?.pushViewController(control, animated: false)
    }
    @IBAction func onSearch(_ sender: Any) {
    
              if self.searchTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).count > 0{
                 // if self.categoryId == "12"{
                      //self.searchBloodDetails(id: self.categoryId, text: self.searchTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines))
                  //}else{
                
//                searchTextField.text = searchTextField.text?.stringByReplacingOccurrencesOfString(" ", withString: "")
                self.searchPersonDetails(id: self.innerData?.main_cat_id ?? "", text: self.searchTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines))
                 // }
              }
      
    }
    //MARK:- API METHOD
    func bookDetail(id:String,subCategoryId:String,start:String,limite:String) {
            print(id)
            let url = BASE_URL +  API.GET_BOOK_DETAILS + "\(id)" + "&sub_cat_id=" + "\(subCategoryId)" + "&start=" + "\(start)" + "&limit=" + "\(limite)"
            print(url)
            Utility.showIndecator()
            CategoryApiManager.shared.getInnerCategory(url: url, complition: { (categoryData) in
                Utility.hideIndicator()
                print(categoryData)
                 self.booksArray.append(contentsOf: categoryData.result!)
                 self.bookDetailTableView.reloadData()
//
                
//                if self.start > 1{
//                    self.appendDataCollectionView(data: categoryData.result!)
//                }else{
//                    self.booksArray = categoryData.result!
//                    self.bookDetailTableView.reloadData()
//                }
                
               }) { (msg) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: msg)
            }
        }
    
    func searchPersonDetails(id:String,text: String) {
              print(id)
        
       // https://pallisseri.com/eps/ios-api/ios/149&searchtag=%E0%B4%AE%E0%B4%BE%E0%B4%A8%E0%B4%B5%E0%B4%BF%E0%B4%95%E0%B4%A4
        let url = BASE_URL +  API.SEARCH_BOOK_DETAIL + "\(id)&searchtag="+text
              print(url)
              Utility.showIndecator()
              CategoryApiManager.shared.getInnerCategory(url: url, complition: { (categoryData) in
                  Utility.hideIndicator()
                  print(categoryData)
                  self.searchCategoryArray = categoryData.result!
                  self.bookDetailTableView.reloadData()
                
                
              }) { (msg) in
                  Utility.hideIndicator()
                  Utility.showAlert(vc: self, message: msg)
              }
          }
    
    
    func appendDataCollectionView(data: [InnerData]){
        let data = data
        var indexPathArray: [IndexPath] = []
        for i in self.booksArray.count..<self.booksArray.count + data.count{
            indexPathArray.append(IndexPath(item: i, section: 0))
        }
        self.booksArray.append(contentsOf: data)
        self.bookDetailTableView.insertRows(at: indexPathArray, with: .automatic)

        
    }
    func showWhatsAppNum() {
        let url = "https://pallisseri.com/eps/ios-api/ios/getLibraryContactNumber.php"
        print(url)
        Utility.showIndecator()
        CategoryApiManager.shared.getInnerCategory(url: url, complition: { (newsData) in
            Utility.hideIndicator()
            print("ContactNumber",newsData)
//            self.navigationController?.popViewController(animated: true)
            //self.dataTableView.reloadData()
           
            if let phoneNumber =  newsData.result?[0].contact_number1{
                Utility.openChatWhatsapp(phone: phoneNumber)
            }
        }) { (msg) in
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: msg)
        }
    }
}
extension BookDetailScreen : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.searchTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).count > 0{
            return self.searchCategoryArray.count
        } else{
            return booksArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookDetailCell", for: indexPath) as! BookTableViewCell
        let model = self.searchCategoryArray.count > 0 ? self.searchCategoryArray[indexPath.row] : booksArray[indexPath.row]
        
        cell.regNoLabel.text = "REG NO " + "\(model.Reg_Number ?? "")"
        cell.authorLabel.text = model.author ?? ""
        cell.bookNameLabel.text = model.book_name ?? ""
        cell.orderNowButton.tag = indexPath.row
        cell.orderNowButton.addTarget(self, action: #selector(onOrder(sender:)), for: .touchUpInside)

        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.searchCategoryArray.isEmpty{
                 if self.booksArray.count - 3 == indexPath.row{
                     self.start += 15
                     bookDetail(id: innerData?.main_cat_id ?? "", subCategoryId: innerData?.sub_cat_id ?? "", start: "\(start)", limite: "15")
                 }
             }
    }
    @objc func onOrder(sender: UIButton){
        let buttonTag = sender.tag
        let model = self.searchCategoryArray.count > 0 ? self.searchCategoryArray[buttonTag] : booksArray[buttonTag]
        let regNum = model.Reg_Number
        showWhatsAppNum()
    }
}
