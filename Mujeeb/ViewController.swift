//
//  ViewController.swift
//  Mujeeb
//
//  Created by iroid on 01/11/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var timer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()
          timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(initializedDetails), userInfo: nil, repeats: false)
    }

    // MARK: - Methods
    @objc func initializedDetails(){
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "HomeScreen") as! HomeScreen
        self.navigationController?.pushViewController(control, animated: true)
    }
}

