//
//  GlobalConstant.swift
//  Iroid
//
//  Created by iroid on 30/03/18.
//  Copyright © 2018 iroidiroid. All rights reserved.
//

import Foundation
import UIKit


let ENGLISH_LANG_CODE         =              "en"
let ARABIC_LANG_CODE         =              "ar"
var tabBarPluesbutton = UIButton.init(type: .custom)
var isButtonClicked = false
var currentLatitude = ""
var currentLongitude = ""
var usersLatitude = ""
var usersLongitude = ""
var userAddress = ""
var currentChatUserId = ""
let deliveryFees = 4.99
var isInChatList = false
var collectionViewHeight = Int()
var globalEventArray = NSMutableArray()
var globalFriendId = NSString()
var isChatTab = false
var isSearchTab = false
var isRegistration = NSString()
var isNewMessageDot = false
var isTownviewBot = false
var isNewFriendDot = false
var isNewNotificationDot = false
//var isDiscoverTab = false
var isFilterOpen = false
var isTodayEventTab = false
var shareTitle: String = ""
var shareUrl : String = ""
var isUserProfile = false
var isEventScroll = false
var isnowEventScrolling = false
var isViewNowEventLayout = false
var isViewUserEventLayout = false
var scrollViewConst = Int()
var isMakeFilter = false
var globalSaveEventArray = NSMutableArray()
var globalcategoryDictionary = NSDictionary()
var productsArray = NSMutableArray()
var isOneTimeServiceCall = false
var isNowEventClick = false
var isDasboard = false
var isOrderPlaced = false
var isShopOpen = false
var isInterestSelected = false
var checkMainScreenService = false
var isFromMyQuestion = Bool()
var isSameUserId = false
var otherUser = String()
var notificationDictionary = NSDictionary()
var isCheckNotification = false
var isFromVehicleDetails = Bool()
var notificationCheck = false
var isFromMenu = Bool()

var isGuest = Bool()
var IsStoreUser = Bool()
var IsStoreUserVerified = Bool()
var IsUser = Bool()
var IsSocialLogIn = Bool()

var CITY_STRING         = String()
var COUNTRY_STRING      = String()
var LATITUDE_STRING     = String()
var LONGITUDE_STRING    = String()
var C_LATITUDE_STRING     = String()
var C_LONGITUDE_STRING    = String()
var cityName = ""
var countryName = ""
var AddPostCityName = ""
var addPostCountryName = ""
var isForStore = Bool()
var isForAccessories = Bool()

var isFromNewCar    = Bool()
var isFromUsedCar   = Bool()
var isFromNewBike   = Bool()
var isFromUsedBike  = Bool()

let DEVICE_UNIQUE_IDETIFICATION : String = UIDevice.current.identifierForVendor!.uuidString
let TEST = "dnmdmnm"
let APPLICATION_NAME = "എന്റെ പള്ളിശ്ശേരി"

//let BASE_URL = "http://aumentaapps.com/museo/api/api.php"

//let BASE_URL = "https://www.iroidsolutions.com/projects/rejoinder/api/api.php"
//let BASE_URL = "https://aumentaapps.com/rejoin/api/api.php"
//let BASE_URL = "http://zag.onf.mybluehost.me/rejoiapp/api/api.php"
//let BASE_URL = "https://expresslaundrykerala.com/eps/ios-api/ios/"
let BASE_URL = "https://pallisseri.com/eps/ios-api/ios/"
let ICON_URL = "https://pallisseri.com/eps/eps-admin/"
let API_ID_VALUE = "f63ab8e4dc88fcda0826a2f695bfd7ba"
let API_SECRET_VALUE = "1c4a417ce28bb18256ca150e4e8d3c6f"

let CART_PRODUCTS   = "cart_products"
let GLOBAL_CATEGORY_MUTABLEARRAY = "globalcategoryMutableArray"
let GLOBAL_CITY_DATA = "GLOBAL_CITY_DATA"
var CHECK_CITY_SERVICE = true
let USER_DETAILS = "user_details"
let PRODUCT_DETAILS = "product_details"
let SUB_CATEGORY_DETAILS = "sub_category_details"
let IS_LOGIN = "is_login"
let IS_GUEST_USER = "is_guest_user"
let CATEGORY = "category"
let CITIES = "cities"
let CITY_NAME = "city"
let COUNTRY_NAME = "country"
let KEYWORD     =  "keyword"
let IS_CALSPAN = "is_calspan"
let ACCESS_KEY  =                 "deer"
let ACCESS_TOKEN =                "access_token"

let AUTHORIZATION  =              "Authorization"
let DEVICE_TOKEN =                "DeviceToken"
let DEVICE_ID =                   "DeviceId"
let DEVICE_TYPE    =              "DeviceType"
let SECRET         =              "secret"
let CATEGORY_NAME   =             "category_name"
let CATEGORY_ID      =            "category_id"
let SUB_CATEGORY_ID   =             "sub_category_id"
let PRODUCT_ID   =             "product_id"
let LAST_PRODUCT_ID   =             "last_product_id"
let TYPE                =           "type"
let ORDER_COMMENT                =           "order_comment"
let ORDER_COMMENT_TIME         =         "order_comment_time"
let CERTIFICATE_TYPE =                 "certificate_type"
let R_DEVICE_TOKEN          =             "DeviceToken"

let EMAIL_ID                =           "email_id"
let PASSWORD                =           "password"
let NAME                    =           "Name"
let USERNAME                =           "UserName"
let FULLNAME                =           "full_name"
let SURNAME                 =           "Surname"
let DATE_OF_BIRTH           =           "DateOfBirth"
let GENDER                  =           "Gender"
let DISTANCE                =           "distance"
let TOTAL_LIKES             =           "total_likes"
let SCORE                   =           "score"
let SOCIAL_ID               =           "social_id"
let SOCIAL_TYPE             =           "social_type"
let TOTAL_RESPONSE          =           "total_response"
let GET_SENDER_ID           =           "SenderId"
let GET_RECIEVER_ID         =           "ReceiverId"
let GET_DIALOG_ID           =           "DialogId"
let GET_MESSSAGE            =           "Message"
let QUESTION_Id             =           "question_id"
let QUESTION_DATA           =           "que_data"
let QUESTION                =           "question"
let UPDATED_AT              =           "updated_at"
let GET_NOTIFICATION_TITLE  =           "Title"
let TOTAL_COMMENTS          =           "total_comments"
let COMMENTS                =           "comments"
let COMENT_DISCRIPTION      =            "comment"
let TOTAL_COMMENT_LIKES     =           "total_comment_likes"
let Is_COMMENT_LIKE_BY_ME   =           "is_comment_like_by_me"
let  IS_FAVORITE_BY_ME      =            "is_favorite_by_me"
let IS_LIKE_BY_ME           =           "is_liked_by_me"
let IS_DISLIKED_BY_ME       =           "is_disLiked_by_me"
let GET_TYPE                =           "Type"
let GET_LIMIT               =           "limit"
let OTHER_USER_ID           =            "other_user_id"
let GET_OFFSET              =           "offset"
let FILTER_OPTION           =            "filter_option"
let COMMENT_ID              =           "comment_id"
let TOTAL_DISLIKES          =            "total_dislikes"
let IS_FLAG_BY_ME           =             "is_flag_by_me"
var cityListView            =             "3"
let FOLLOWING               =           "following"
let SUBJECT                 =           "subject"
let OLD_PASSWORD            =           "old_password"
let NEW_PASSWORD            =            "new_password"

//let GET_VEHICLE_  = "VehicleImage1": "ImageData",
//let GET_VEHICLE_  = "VehicleImage2": "ImageData"
//=
let SEARCH                  =             "0"

let MORE_LOOKING            =             "2"
let SEASON_PRODUCTS         =             "3"
let OFFERS_OF_THE_DAY       =             "4"
let RELATED_PRODUCT         =             "5"
let BOUGHT_PRIVIOUSLY       =             "6"
let CUSTOMER_BOUGHT         =             "7"
let RECOMMENDED             =             "8"
let LOYALTY                 =             "9"

let USER_ID                 =             "user_id"
let FOLLOWER_ID            =             "follower_id"
let CURRENT_PASSWORD        =             "CurrentPassword"
let AVAILABLE_TIME          =             "available_time"
let IS_ANSWERED             =             "is_answered"

let EMAIL                   =           "Email"
let ID                      =           "id"
let PICURL                  =           "url"
let PHONE_NO                =           "phone_no"
let WEBSITE                 =               "website"
let SEARCH_TEXT             =             "serarch_text"
let LANGUAGE_TYPE           =             "language_type"
let ADDRESS      =                          "address"
let SCHEDULE      =                          "schedule"
let CLOSED                          =               "closed"
let ADMISSION                          =               "admission"
let NOTE                            =                   "note"
let INSTAGRAM_LINK                  =               "instagram_link"
let FACEBOOK_LINK                  =               "facebook_link"
let TWITTER_LINK                  =               "twitter_link"
let PINTEREST_LINK                  =               "pinterest_link"
let YOUTUBE_LINK                  =               "youtube_link"
let OTHER_MUSEUM                    =                   "other_museum"
var IS_OTHER_PROFILE = false 

let PICTURE                       =           "picture"

// Extras

let TAG_ID  =         "tag_id"
let TAG_ICON  =         "tag_icon"
let TAG_ICON_GRAY  =         "tag_icon_grey"
let TAG_TITLE  =         "tag_title"
let IS_INTRESTED_TAG  =         "is_intrested_tag"

let POST_ID  =         "post_id"
let POST_VIEW  =         "post_view"
let POST_DISTANCE  =         "post_distance"
let INTRESTED_TAG  =         "interested_tag"
let VERIFICATION_CODE   =            "verification_code"

let MESSAGE               =                  "message"
let MESSAGE_ID            =                  "message_id"
let MESSAGE_COUNT         =                  "message_count"
let LAST_MESSAGE          =                  "last_message"
let HAS_NEW_MESSAGE_STATUS =                  "has_new_message_status"
let IS_SEEN               =                   "is_seen"
let FRIENDS_COUNT         =                   "friends_count"
let IS_MUTE               =                  "is_mute"

let USER_NAME = "user_name"
let TIME_ZONE = "time_zone"

let TIMEZONE = "timezone"

let FRIEND_STATUS = "friend_status"
let REQESTED_TO = "requested_to"
let ACTION = "action"
let VIEW = "view"
let USERS                 =             "users"
let MOST_RECENT                 =             "most_recent"
let RECEIVER_ID                 =             "receiver_id"
let COMMENT                 =             "comment"
let STATUS   = "status"
let PROFILE_PRIVACY = "profile_privacy"
let NOTIFICATION_ID = "notification_id"
let NOTIFICATION_TYPE        =    "notification_type"
let SENDER_ID                 =             "sender_id"
let RECEIVER_IDS              =             "receiver_id"


let USER_ADDRESS_ID                 =             "user_address_id"

let PRODUCT_REMINDER_NOTIFICATION           =             "PRODUCT_REMINDER_NOTIFICATION"


let FIRST_NAME =                 "first_name"
let LAST_NAME =                  "last_name"
let CREATE_AT =                  "created_at"
let PROFILE_PIC =                "profile_pic"
let PRODUCT_TITLE =              "product_title"
let PRICE_1         =            "price_1"
let UNIT =                       "unit"
let IS_SELECTED = "is_selected"

let SERVER_DATE_FORMATE     =    "yyyy-MM-dd HH:mm:ss"
let TIME_FORMATE     =           "hh:mm a"

let LATITUDE =                     "latitude"
let LONGITUDE =                     "longitude"
let TRANSPORT_TITLE     =                  "transport_title"
let TRANSPORT_ADDRESS   =                  "transport_address"
let PARKING_TITLE       =                  "parking_title"
let PARKING_ADDRESS     =                  "parking_address"


let VIDEOS  =         "videos"
let VIDEO_TITLE  =         "video_title"

let AREA_NAME       =       "area_name"
let AREA_ID        =       "area_id"

let MUSEUM             =     "museum"
let NUMBER_OF_MUSEUM       =     "number_of_museum"
let MUSEUM_ID    =    "museum_id"
let IMAGE      =        "image"
let TITLE      =        "title"
let SHORT_DESC      =        "short_description"
let START_TIME      =        "start_time"
let END_TIME      =        "end_time"

let DESCRIPTION =       "description"
let SUPER_LIST_TITLE =           "super_list_title"
let SUPER_LIST_ID =              "super_list_id"

let SUPER_ITEM_TITLE =           "super_item_title"
let SUPER_ITEM_ID    =           "super_item_id"
let POINTS    =                   "points"

let SPONSOR_ID  =         "sponsor_id"
let SPONSOR_IMAGE  =         "sponsor_image"
let SPONSOR_LINK  =         "sponsor_link"

let ORGANIZER_ID  =         "organizer_id"
let ORGANIZER_IMAGE  =         "organizer_image"
let ORGANIZER_LINK  =         "organizer_link"

let API_ID = "api_id"
let API_SECRET = "api_secret"
let API_REQUEST = "api_request"
let DATA = "data"
let ARCHIVEMENETS = "achivements"
let COMMAND = "command"
let MSG = "msg"
let TAG = "tag"
let SOCIAL_LOGIN_DETAILS = "social_login_DETAILS"
let BANNER_IMAGE = "banner_image"

let IS_FIRST_TIME_INSTALL_APP = "is_first_time_install"
let FIRST_TIME_INSTALL = "First_time_install"
let FLAG = "flag"
let GET_CITY_NAME = "GET_CITY_NAME"
let GET_COUNTRY_NAME = "GET_COUNTRY_NAME"

let QUESTION_ANS_UPDATE_DETAIL = "QUESTION_ANS_UPDATE_DETAIL"
let QUESTION_ANS_UPDATE = "QUESTION_ANS_UPDATE"
let MY_QUESTION_ANS_UPDATE = "MY_QUESTION_ANS_UPDATE"
let DICTIONARY_UPDATE = "dict_update"
let MY_QUESTION_UPDATE = "MY_QUESTION_UPDATE"
let SESSTION_EXPIRE    =      "sesstionExpire"
let MY_QUESTION_DICTIONARY_UPDATE = "MY_QUESTION_DICTIONARY_UPDATE"

let SHOW_USER = "show_user"
let ON_TYPE_5 = "on_type_5"
let NOTIFICATI_LIST = "notification_list"
let REFRESH_FOLLOWING = "refresh_following"
let REFRESH_FOLLOWER = "refresh_follower"
let LANGUAGE = "language"


struct API
{
    static var GET_TAXI_CATEGORY  =  "taxicategory.php"
    static var SUB_CATEGORY  =  "ListSubCategory.php?main_cat_id="
     static var INNER_SUB_CATEGORY  =  "ListMinorCategory.php?sub_cat_id="
    static var INNER_SUB_CATEGORY_NEW = "getContactDetails.php?sub_cat_id="
     static var MAIN_CATEGORY  =  "ListSubCategory.php?main_cat_id="
    static var GET_CONTACT_DETAIL  =  "getContactDetails.php?sub_cat_id="
    static var TAXI_DETAILS = "TaxiDetails.php?id="
    static var GET_BUS_TRAIN_DETAIL = "getBusTrainDetails.php?main_cat_id="
    static var GET_NOTICE_DETAILS = "getNoticeDetails.php?main_cat_id="
     static var GET_BOOK_DETAILS = "getBookDetails.php?main_cat_id="
    static var GET_PERSON_DETAILS = "getPersonBasicDetails.php?main_cat_id="
    static var GET_SEARCH_PERSON_DETAILS = "srchPersonDetails.php?main_cat_id="
    static var SEARCH_BOOK_DETAIL = "srchBookDetails.php?main_cat_id=14&sub_cat_id="
    static let GET_BLOOD_DETAILS = "getBloodDonerDetails.php?main_cat_id="
    static let SEARCH_BLOOD_DETAILS = "searchDoner.php?main_cat_id="
//    static let GET_ADBOX_SUBPAGE = "getAdboxSubPage.php?main_cat_id=1&page_id=2"
}

struct API_INSTGRAM
{
    static var INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    static var INSTAGRAM_CLIENT_ID = "777a117d15cf4f6a866c0495eea80edf"
    static var INSTAGRAM_CLIENTSERCRET = "e6fa86f36b744651a72ac9be8f232516"
    static var INSTAGRAM_REDIRECT_URI = "http://www.iroid-apps.com/"
    static var INSTAGRAM_ACCESS_TOKEN = "access_token"
    static let INSTAGRAM_SCOPE = "likes+comments+relationships"
    /* add whatever scope you need https://www.instagram.com/developer/authorization/ */
}


// **********************************
//            fileManagers
// **********************************

let PLIST_FILE_OTHER_SETTINGS = "OtherSettings.plist"
