//
//  CommanModel.swift
//  Mujeeb
//
//  Created by iroid on 05/11/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import Foundation


struct LoginNotification: Codable {
    let desktop: Bool
    let email: Bool
    let text: Bool
}

struct LoginFailure: Codable {
    let message: String?
    let errors: LoginError?
    let error: String?
}

struct LoginError: Codable {
    let email: [String]?
    let password: [String]?
}
