//
//  SoundDetailTableViewCell.swift
//  Mujeeb
//
//  Created by iroid on 03/12/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class SoundDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
