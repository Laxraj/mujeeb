//
//  BloodCollectionViewCell.swift
//  Mujeeb
//
//  Created by iroid on 27/12/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class BloodCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
