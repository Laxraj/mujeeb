//
//  ContactUSTableViewCell.swift
//  Mujeeb
//
//  Created by iroid on 01/12/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class ContactUSTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var mobileNumberLabel: UILabel!
    @IBOutlet weak var phoneNumberButton: UIButton!
    @IBOutlet weak var onWhatssapp: UIButton!
    @IBOutlet weak var onShare: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var infoLAbel: UILabel!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoViewHeight: NSLayoutConstraint!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var locationViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var secoundMobileNumberView: UIView!
    @IBOutlet weak var secondMobileViewHeight: NSLayoutConstraint!
    @IBOutlet weak var secondMobileNumLabel: UILabel!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lineLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
