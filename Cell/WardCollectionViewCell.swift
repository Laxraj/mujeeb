//
//  WardCollectionViewCell.swift
//  Mujeeb
//
//  Created by iroid on 26/12/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class WardCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var userNameLAbel: UILabel!
    @IBOutlet weak var addessLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var wardLabel: UILabel!
    @IBOutlet weak var onCall: dateSportButton!
    @IBOutlet weak var onWhatsapp: dateSportButton!
    @IBOutlet weak var onShare: dateSportButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
