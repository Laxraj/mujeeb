//
//  NewsDetailsTableViewCell.swift
//  Mujeeb
//
//  Created by Kishan on 21/11/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class NewsDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var newsHeadLineLabel: UILabel!
    @IBOutlet weak var newsDiscriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
