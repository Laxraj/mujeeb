//
//  InnerCategoryCollectionViewCell.swift
//  Mujeeb
//
//  Created by Kishan on 02/11/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class InnerCategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
