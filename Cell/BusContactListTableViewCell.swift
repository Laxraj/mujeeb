//
//  BusContactListTableViewCell.swift
//  Mujeeb
//
//  Created by iroid on 03/12/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class BusContactListTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLAbel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var detailViewHeight: NSLayoutConstraint!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var mainViewHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
