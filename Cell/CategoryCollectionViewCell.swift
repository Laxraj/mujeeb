//
//  CategoryCollectionViewCell.swift
//  Mujeeb
//
//  Created by iroid on 01/11/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoryBackgroundView: dateSportView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
