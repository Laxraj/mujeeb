//
//  DownloadPDFCell.swift
//  Mujeeb
//
//  Created by iroid on 24/01/21.
//  Copyright © 2021 iroid. All rights reserved.
//

import UIKit

class DownloadPDFCell: UICollectionViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var downLoadButton: dateSportButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   
}
