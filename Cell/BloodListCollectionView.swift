//
//  BloodListCollectionView.swift
//  Mujeeb
//
//  Created by iroid on 27/12/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class BloodListCollectionView: UICollectionViewCell {

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var groupLabel: UILabel!
    @IBOutlet weak var onCall: dateSportButton!
    @IBOutlet weak var onWhatsapp: dateSportButton!
    @IBOutlet weak var onShare: dateSportButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
