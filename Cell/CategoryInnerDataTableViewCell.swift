//
//  CategoryInnerDataTableViewCell.swift
//  Mujeeb
//
//  Created by iroid on 03/11/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import UIKit

class CategoryInnerDataTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var mobileNumberLabel: UILabel!
    @IBOutlet weak var categoryImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
